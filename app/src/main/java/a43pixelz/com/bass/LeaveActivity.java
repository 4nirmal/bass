package a43pixelz.com.bass;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import a43pixelz.com.bass.pojo.Leave.Leave;
import a43pixelz.com.bass.pojo.Leave.LeaveBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeaveActivity extends AppCompatActivity {
    SPref sPref;
    Calendar myCalendar;
    Calendar myCalendar2;
    EditText todate;
    EditText fromdate;
    EditText leavereason;
    Button applyleave;
    Button cancelleave;
    TextView leaveid;
    TextView leavestatus;
    TextView leavecommment;
    TextView leavedays;
    TextView leaveuser;
    TextView user_id;
    a43pixelz.com.bass.WrapListView historylistview;
    TextView updateddby;
    TextView leaveentity;
    int editable=1;
    APIInterface apiInterface;
    ArrayList<ListDataModel_complete_leave> dataModels;
    CustomAdapter_complete_leave adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave);
        sPref=new SPref(this);

        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        myCalendar2 = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar2.set(Calendar.YEAR, year);
                myCalendar2.set(Calendar.MONTH, monthOfYear);
                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel2();
            }

        };
       
        todate=findViewById(R.id.leavefrom);
        fromdate=findViewById(R.id.leaveto);
        leavereason=findViewById(R.id.leavereason);
        leavestatus=findViewById(R.id.leavestatus);
        leavecommment=findViewById(R.id.leavecomment);
        leavedays=findViewById(R.id.noofdays);
        leaveid=findViewById(R.id.leaveid);
        applyleave=findViewById(R.id.leaveapply);
        cancelleave = findViewById(R.id.cancelleave);
        user_id=findViewById(R.id.leaveuser);
        updateddby=findViewById(R.id.leaveupdatedby);
        leaveentity=findViewById(R.id.leaveentity);
        leaveuser=findViewById(R.id.leaveuser);
        historylistview=findViewById(R.id.history_listview);
        cancelleave=findViewById(R.id.cancelleave);
        Bundle extra=getIntent().getExtras();

        apiInterface=APIClient.getClient().create(APIInterface.class);
        if(extra!=null){
            if(!extra.getString("leave_id").matches("")){
                leaveid.setText(extra.getString("leave_id"));
                try{
                Call<Leave> call = apiInterface.getLeaveById(extra.getString("leave_id"));
                call.enqueue(new Callback<Leave>() {
                    @Override
                    public void onResponse(Call<Leave> call, Response<Leave> response) {
                        LeaveBody data = response.body().getBody().get(0);
                        user_id.setText(data.getUserid());
                        leaveuser.setText(data.getUsername());
                        todate.setText(data.getTo());
                        fromdate.setText(data.getFrom());
                        leavereason.setText(data.getReason());
                        leavestatus.setText(data.getStatus());
                        leavecommment.setText(data.getComment());
                        leaveentity.setText(data.getEntityid());
                        updateddby.setText(data.getUsername());
                        applyleave.setVisibility(View.INVISIBLE);
                        if(data.getStatus().matches("Requested")){
                        cancelleave.setVisibility(View.VISIBLE);}
                        getSupportActionBar().setTitle("Leave ID: "+extra.getString("leave_id"));
                        getSupportActionBar().setSubtitle(data.getUsername());
                    }
                    @Override
                    public void onFailure(Call<Leave> call, Throwable t) {

                    }
                });}catch (Exception exe){

                }
            }else{
                getSupportActionBar().setTitle("Apply Leave");
                applyleave.setVisibility(View.VISIBLE);
                cancelleave.setVisibility(View.INVISIBLE);
                user_id.setText(sPref.getValue("user_id"));
                leaveuser.setText(sPref.getValue("user_name"));
                leaveentity.setText(sPref.getValue("user_entity"));
                updateddby.setText(sPref.getValue("user_name"));
            }
        }else{
            getSupportActionBar().setTitle("Apply Leave");
            applyleave.setVisibility(View.VISIBLE);
            cancelleave.setVisibility(View.INVISIBLE);
            user_id.setText(sPref.getValue("user_id"));
            leaveuser.setText(sPref.getValue("user_name"));
            leaveentity.setText(sPref.getValue("user_entity"));
            updateddby.setText(sPref.getValue("user_name"));


        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        try{
        Call<Leave> call2 = apiInterface.doGetListLeave();
        call2.enqueue(new Callback<Leave>() {
            @Override
            public void onResponse(Call<Leave> call2, Response<Leave> response) {
                Log.d("TAG",response.code()+"");
                dataModels = new ArrayList<>();
                String displayResponse = "";
                Leave resource = response.body();
                Integer count = resource.getCount();
                List<LeaveBody> datumList = resource.getBody();


                for (LeaveBody datum : datumList) {
                    //todo changes here
                    if (datum.getUserid().matches(user_id.getText().toString())) {
                        String name = datum.getUserid();
                        String id = datum.getLeaveid();
                        String reason = datum.getReason();
                        String updatedby = datum.getStatus()+" by "+datum.getUpdatedBy();
                        String comment = datum.getComment();
                        String date2 = datum.getFrom()+" to "+datum.getTo();
                        dataModels.add(new ListDataModel_complete_leave(name, id, date2, reason,0,updatedby,comment));
                    }
                }

                adapter= new CustomAdapter_complete_leave(dataModels,LeaveActivity.this);

                historylistview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                historylistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent LeaveView=new Intent(LeaveActivity.this,LeaveActivity.class);
                        ListDataModel_complete_leave dataModel= dataModels.get(position);
                        LeaveView.putExtra("leave_id", dataModel.getLeaveId());
                        startActivity(LeaveView);

                    }
                });
            }
            @Override
            public void onFailure(Call<Leave> call, Throwable t) {
                call.cancel();
            }
        });}catch (Exception exe){

        }
        applyleave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String from = fromdate.getText().toString();
                String to = todate.getText().toString();
                String reason = leavereason.getText().toString();
                String userid = user_id.getText().toString();
                String status = "Requested";
                String updatedby=updateddby.getText().toString();
                String entityid=leaveentity.getText().toString();
                String leaveiid=leaveid.getText().toString();
                String comment=leavecommment.getText().toString();
                String username=leaveuser.getText().toString();

                LeaveBody u = new LeaveBody();
                u.setFrom(from);
                u.setTo(to);
                u.setComment(comment);
                u.setEntityid(entityid);
                u.setReason(reason);
                u.setStatus(status);
                u.setUpdatedBy(updatedby);
                u.setLeaveid(leaveiid);
                u.setUserid(userid);
                u.setUsername(username);
                try{
                Call<LeaveBody> call = apiInterface.createLeave(u);
                call.enqueue(new Callback<LeaveBody>() {
                    @Override
                    public void onResponse(Call<LeaveBody> call, Response<LeaveBody> response) {
                        Toast.makeText(getApplicationContext(), "Leave Requested successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<LeaveBody> call, Throwable t) {
                        Log.e("puss",t.getMessage());
                    }
                });}catch (Exception exe){

                }
            }
        });
        cancelleave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String from = fromdate.getText().toString();
                String to = todate.getText().toString();
                String reason = leavereason.getText().toString();
                String userid = user_id.getText().toString();
                String status = "Canceled";
                String updatedby=updateddby.getText().toString();
                String entityid=leaveentity.getText().toString();
                String leaveiid=leaveid.getText().toString();
                String comment=leavecommment.getText().toString();
                String username=leaveuser.getText().toString();

                LeaveBody u = new LeaveBody();
                u.setFrom(from);
                u.setTo(to);
                u.setComment(comment);
                u.setEntityid(entityid);
                u.setReason(reason);
                u.setStatus(status);
                u.setUpdatedBy(updatedby);
                u.setLeaveid(leaveiid);
                u.setUserid(userid);
                u.setUsername(username);
                try{
                Call<LeaveBody> call = apiInterface.updateLeave(u);
                call.enqueue(new Callback<LeaveBody>() {
                    @Override
                    public void onResponse(Call<LeaveBody> call, Response<LeaveBody> response) {
                        Toast.makeText(getApplicationContext(), "Leave canceled successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<LeaveBody> call, Throwable t) {

                    }
                });}catch (Exception exe){

                }
            }
        });
        todate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editable!=0) {
                    new DatePickerDialog(LeaveActivity.this, date2, myCalendar2
                            .get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH),
                            myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });
        fromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editable!=0) {
                    new DatePickerDialog(LeaveActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }

            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        fromdate.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateLabel2() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        todate.setText(sdf.format(myCalendar2.getTime()));
    }
}
