package a43pixelz.com.bass;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import a43pixelz.com.bass.pojo.Shift.Shift;
import a43pixelz.com.bass.pojo.Shift.ShiftBody;
import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Intent userUpdate;
    APIInterface apiInterface;
    ArrayList<ListDataModel_user> dataModels;
    ListView listView;
    private static CustomAdapter_user adapter;
    String Entity_ID;
    SPref sPref;
    AlertDialog alert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Admin Home");
        sPref=new SPref(AdminHomeActivity.this);



        dialog =new ProgressDialog(this);
        dialog.setMessage("Loading Please wait");
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        TextView entity = (TextView) headerView.findViewById(R.id.admin_entity);
        entity.setText(sPref.getValue("admin_entity"));
        TextView name = (TextView) headerView.findViewById(R.id.admin_name);
        name.setText(sPref.getValue("admin_name"));

        userUpdate = new Intent(this, CalenderViewActivity.class);

        listView=(ListView)findViewById(R.id.user_list);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView2 = li.inflate(R.layout.shiftpopup, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView2);

        alert = alertDialogBuilder.create();
        alert.setCancelable(true);
        myCalendar = Calendar.getInstance();
        /**
         GET List Resources
         **/
        Entity_ID="";
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            Entity_ID=extras.getString("entity_id");
        }
        else {

        }
        try{
        Call<User> call = apiInterface.doGetListUser();
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d("TAG",response.code()+"");
                dataModels = new ArrayList<>();
                String displayResponse = "";
                User resource = response.body();
                Integer count = resource.getCount();
                List<UserBody> datumList = resource.getBody();

                if(Entity_ID=="") {
                for (UserBody datum : datumList) {
                    String name = datum.getName();
                    String id = datum.getUserid();
                    dataModels.add(new ListDataModel_user(name, id, "", ""));
                }
                }else{
                    for (UserBody datum : datumList) {
                        if(datum.getUserid().matches(Entity_ID)) {
                            String name = datum.getName();
                            String id = datum.getUserid();
                            dataModels.add(new ListDataModel_user(name, id, "", ""));
                        }
                    }
                }
                adapter= new CustomAdapter_user(dataModels,getApplicationContext());
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        ListDataModel_user dataModel= dataModels.get(position);
                        userUpdate.putExtra("user_id", dataModel.getId());
                        startActivity(userUpdate);
                    }
                });
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                call.cancel();
                Toast.makeText(AdminHomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });}catch (Exception exe){

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    ProgressDialog dialog;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin_home, menu);
        return true;
    }
    Calendar myCalendar;
    private void getpopupview() {
        EditText from=(EditText)alert.findViewById(R.id.shiftfrom);
        EditText to=(EditText)alert.findViewById(R.id.shiftto);
        Button add=(Button)alert.findViewById(R.id.add_shift);
        final TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(alert.getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                from.setText( selectedHour + ":" + selectedMinute);
            }
        }, myCalendar.get(Calendar.HOUR), myCalendar.get(Calendar.MINUTE), false);
        final TimePickerDialog mTimePicker2;
        mTimePicker2 = new TimePickerDialog(alert.getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                to.setText( selectedHour + ":" + selectedMinute);
            }
        }, myCalendar.get(Calendar.HOUR), myCalendar.get(Calendar.MINUTE), false);
        from.setOnClickListener(v -> {
            mTimePicker.setTitle("Select Shift START time");
            mTimePicker.show();
        });
        to.setOnClickListener(v -> {
            mTimePicker2.setTitle("Select Shift END time");
            mTimePicker2.show();
        });
        add.setOnClickListener(v -> {
            alert.dismiss();
            dialog.show();
            try{
                Call<Shift> call = apiInterface.doGetListShift();
                call.enqueue(new Callback<Shift>() {
                    @Override
                    public void onResponse(Call<Shift> call, Response<Shift> response) {
                        Shift resource = response.body();
                        List<ShiftBody> datumList = resource.getBody();
                        int already=0;
                        for (ShiftBody datum : datumList) {
                            if(datum.getShiftFrom().matches(from.getText().toString())&&datum.getShiftTo().matches(to.getText().toString())){
                                already=1;
                                break;
                            }
                        }
                        if(already==0){
                            ShiftBody u = new ShiftBody();
                            u.setShiftFrom(from.getText().toString());
                            u.setShiftTo(to.getText().toString());
                            try{
                                Call<ShiftBody> call2 = apiInterface.createShift(u);
                                call2.enqueue(new Callback<ShiftBody>() {
                                    @Override
                                    public void onResponse(Call<ShiftBody> call2, Response<ShiftBody> response) {
                                        Toast.makeText(AdminHomeActivity.this, "New Shift Timing Added", Toast.LENGTH_LONG).show();
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onFailure(Call<ShiftBody> call2, Throwable t) {
                                        dialog.dismiss();
                                    }
                                });
                            }catch (Exception exe){

                            }

                        }else {
                            Toast.makeText(AdminHomeActivity.this, "Same Shift timing found", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }

                    }

                    @Override
                    public void onFailure(Call<Shift> call, Throwable t) {

                        ShiftBody u = new ShiftBody();
                        u.setShiftFrom(from.getText().toString());
                        u.setShiftTo(to.getText().toString());
                        Call<ShiftBody> call2 = apiInterface.createShift(u);
                        call2.enqueue(new Callback<ShiftBody>() {
                            @Override
                            public void onResponse(Call<ShiftBody> call2, Response<ShiftBody> response) {
                                Toast.makeText(AdminHomeActivity.this, "New Shift Timing Added", Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            }

                            @Override
                            public void onFailure(Call<ShiftBody> call2, Throwable t) {
                                dialog.dismiss();
                            }
                        });

                    }
                });
            }catch (Exception exe){

            }
            alert.dismiss();
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            sPref.deleteSpref();
            startActivity(new Intent(AdminHomeActivity.this,LoginActivity.class));
            finish();
            return true;
        }
        else if (id == R.id.add_shift) {
            alert.show();
            getpopupview();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_users_a) {
            //startActivity(new Intent(this, UserListActivity.class));
        }else if (id == R.id.nav_add_users) {
            startActivity(new Intent(this, AddUserActivity.class));
        } else if(id == R.id.nav_leaves_a){
            startActivity(new Intent(this, LeaveManageActivity.class));
        } else if (id == R.id.nav_profile_a) {
            startActivity(new Intent(this, AdminProfileActivity.class));
        }else if (id == R.id.notification) {
            startActivity(new Intent(this, NotificationActivity.class));
        }else if (id == R.id.nav_event) {
            startActivity(new Intent(this, EventActivity.class));
        }
        else if (id == R.id.attendancelist_a){
            startActivity(new Intent(this, AttendanceListActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
