package a43pixelz.com.bass;

/**
 * Created by Nirmal on 9/22/2018.
 */

public class ListDataModel_notify {

    String title;
    String date;
    String message;

    public ListDataModel_notify(String message, String date, String title ) {
        this.message=message;
        this.date=date;
        this.title=title;

    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getmessage() {
        return message;
    }


}