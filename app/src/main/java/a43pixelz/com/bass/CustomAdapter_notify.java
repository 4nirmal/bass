package a43pixelz.com.bass;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import a43pixelz.com.bass.pojo.Admin.Admin;
import a43pixelz.com.bass.pojo.Admin.AdminBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nirmal on 9/22/2018.
 */

public class CustomAdapter_notify extends ArrayAdapter<ListDataModel_notify> {

    private ArrayList<ListDataModel_notify> dataSet;
    Context mContext;


    public CustomAdapter_notify(ArrayList<ListDataModel_notify> data, Context context) {
        super(context, R.layout.notificationitem, data);
        this.dataSet = data;
        this.mContext=context;

    }

    private static class ViewHolder {
        TextView title;
        TextView date;
        TextView message;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
         // Get the data item for this position
        ListDataModel_notify dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.notificationitem, parent, false);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title2);
            viewHolder.date = (TextView) convertView.findViewById(R.id.date2);
            viewHolder.message = (TextView) convertView.findViewById(R.id.message2);
            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        viewHolder.title.setText(dataModel.getTitle());
        viewHolder.date.setText(dataModel.getDate());
        viewHolder.message.setText(dataModel.getmessage());
        // Return the completed view to render on screen
        return convertView;
    }
}
