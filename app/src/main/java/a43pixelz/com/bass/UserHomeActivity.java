package a43pixelz.com.bass;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
//import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.CalendarView;

import a43pixelz.com.bass.pojo.Admin.AdminBody;
import a43pixelz.com.bass.pojo.Atendance.Attendance;
import a43pixelz.com.bass.pojo.Atendance.AttendanceBody;
import a43pixelz.com.bass.pojo.Entity.Body;
import a43pixelz.com.bass.pojo.Entity.Entity;
import a43pixelz.com.bass.pojo.Notification.Notification;
import a43pixelz.com.bass.pojo.Notification.NotificationBody;
import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.util.*;
import java.text.*;

public class UserHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, LocationListener{

    LocationManager locationManager;
    String mprovider;
    APIInterface apiInterface;
    LocationListener locationListener;
    SPref sPref;
    String Latitude, Longitude;
    int active=0;
    ProgressDialog progressDialog;
    Button Checkout,Checkin;
    Boolean checkin,checkout;
    AttendanceBody check;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sPref=new SPref(this);
        GPSTracker gpsTracker = new GPSTracker(this);
        List<EventDay> events = new ArrayList<>();
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Getting Location");
        progressDialog.setCancelable(false);
        Checkin = (Button) findViewById(R.id.checkin_btn);
        Checkout = (Button) findViewById(R.id.checkout_btn);
        CalendarView calendarView = (CalendarView) findViewById(R.id.calendarView);
        check=new AttendanceBody();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        try{
        Call<Attendance> call3 = apiInterface.getAttendanceByUserId(sPref.getValue("user_id"));
        call3.enqueue(new Callback<Attendance>() {
            @Override
            public void onResponse(Call<Attendance> call3, Response<Attendance> response) {
                Log.d("TAG",response.code()+"");
                Attendance resource = response.body();
                List<AttendanceBody> datumList = resource.getBody();


                for (AttendanceBody datum : datumList) {
                    String dateStr = datum.getDatestr();

                    Date date = null;
                    try {
                        date = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(dateStr);
                    } catch (ParseException e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    events.add(new EventDay(cal, R.drawable.adduser));

                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                    String today = dateFormat.format(new Date());
                    if(today.equals(dateStr)){
                        Checkin.setEnabled(false);
                        Checkout.setEnabled(true);
                        check.setIntime(datum.getIntime());
                        check.setUserid(datum.getUserid());
                        check.setDatestr(datum.getDatestr());
                        check.setLocation(datum.getLocation());
                        check.setEntityid(datum.getEntityid());
                        check.setId(datum.getId());
                        check.setbackupLocation(datum.getbackupLocation());
                    }
                }
                calendarView.setEvents(events);

            }

            @Override
            public void onFailure(Call<Attendance> call3, Throwable t) {

            }
        });
        }catch (Exception exe){

        }

        getSupportActionBar().setTitle(sPref.getValue("user_name"));
        getSupportActionBar().setSubtitle(sPref.getValue("user_id"));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView entity = (TextView) headerView.findViewById(R.id.user_entity);
        entity.setText(sPref.getValue("user_entity"));
        TextView name = (TextView) headerView.findViewById(R.id.user_name);
        name.setText(sPref.getValue("user_name"));


        Checkout.setEnabled(false);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    new AlertDialog.Builder(UserHomeActivity.this)
                            .setTitle("Location access not given")
                            .setMessage("Proceed to given permission. ")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Prompt the user once explanation has been shown
                                    ActivityCompat.requestPermissions(UserHomeActivity.this,
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            1);
                                }
                            })
                            .create()
                            .show();
                    return;
                }
                else {
                    if (gpsTracker.getIsGPSTrackingEnabled())
                    {
                        active= 1;
                        checkin=true;
                        checkout=false;
                        progressDialog.show();
                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        if (locationManager != null) {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) UserHomeActivity.this);
                        }
                        if (locationManager != null) {
                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, UserHomeActivity.this);
                        }


                    }
                    else{
                        Toast.makeText(UserHomeActivity.this, "Please enable GPS", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        Checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    new AlertDialog.Builder(UserHomeActivity.this)
                            .setTitle("Location access not given")
                            .setMessage("Proceed to given permission. ")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Prompt the user once explanation has been shown
                                    ActivityCompat.requestPermissions(UserHomeActivity.this,
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            1);
                                }
                            })
                            .create()
                            .show();
                    return;
                }
                else {
                    if (gpsTracker.getIsGPSTrackingEnabled())
                    {
                        active= 1;
                        checkin=false;
                        checkout=true;
                        progressDialog.show();
                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        if (locationManager != null) {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) UserHomeActivity.this);
                        }
                        if (locationManager != null) {
                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, UserHomeActivity.this);
                        }


                    }
                    else{
                        Toast.makeText(UserHomeActivity.this, "Please enable GPS", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            sPref.deleteSpref();
            startActivity(new Intent(UserHomeActivity.this,LoginActivity.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_attendance_u) {
            // Handle the camera action
        } else if (id == R.id.nav_leaves_u) {
            startActivity(new Intent(this,LeaveActivity.class).putExtra("leave_id",""));

        }  else if (id == R.id.nav_profile) {
            startActivity(new Intent(this,UserProfileActivity.class));

        }else if (id == R.id.notification) {
            startActivity(new Intent(this, NotificationActivity.class));
        }else if (id == R.id.nav_event) {
            startActivity(new Intent(this, EventActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("Current Longitude:",  location.getLongitude()+"");
        Log.e("Current Latitude:" ,  location.getLatitude()+"");
        Longitude =String.valueOf(location.getLongitude());
        Latitude=String.valueOf(location.getLatitude());

        if (active!=0&&checkin&&!checkout)
        {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            Date date = new Date();
            AttendanceBody u = new AttendanceBody();
            u.setLocation(Latitude + "," + Longitude);
            Toast.makeText(UserHomeActivity.this, Latitude + " , " + Longitude, Toast.LENGTH_SHORT).show();
            u.setDatestr(dateFormat.format(date));
            u.setEntityid(sPref.getValue("user_entity"));
            u.setIntime(String.valueOf(new java.util.Date().getTime()));
            u.setUserid(sPref.getValue("user_id"));
            u.setStatus("working");
            u.setbackupLocation("");
            u.setOutLocation("");
            u.setOuttime("");
            try{
            Call<AttendanceBody> call = apiInterface.createAttendance(u);
            call.enqueue(new Callback<AttendanceBody>() {
                    @Override
                    public void onResponse(Call<AttendanceBody> call, Response<AttendanceBody> response) {
                        Toast.makeText(UserHomeActivity.this, "Checked IN Successfully", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        Checkin.setEnabled(false);
                        Checkout.setEnabled(true);
                        //finish();
                    }

                    @Override
                    public void onFailure(Call<AttendanceBody> call, Throwable t) {
                        Toast.makeText(UserHomeActivity.this, "Failed"+t.getMessage() , Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
            }catch (Exception exe){

            }
        }
        else if (active!=0&&!checkin&&checkout)
        {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");


            Toast.makeText(UserHomeActivity.this, Latitude + " " + Longitude, Toast.LENGTH_SHORT).show();
            check.setOutLocation(Latitude + "," + Longitude);
            check.setOuttime(String.valueOf(new java.util.Date().getTime()));
            try{
            Call<AttendanceBody> call = apiInterface.updateAttendance(check);
            call.enqueue(new Callback<AttendanceBody>() {
                @Override
                public void onResponse(Call<AttendanceBody> call, Response<AttendanceBody> response) {
                    Toast.makeText(UserHomeActivity.this, "Checked OUT Successfully", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    Checkin.setEnabled(true);
                    Checkout.setEnabled(false);
                    //finish();
                }

                @Override
                public void onFailure(Call<AttendanceBody> call, Throwable t) {
                    Toast.makeText(UserHomeActivity.this, "Failed: "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
            }catch (Exception exe){

            }
        }
        else {
            Toast.makeText(UserHomeActivity.this, "No Location Provider Found.", Toast.LENGTH_SHORT).show();

        }
        locationManager.removeUpdates(UserHomeActivity.this);
        locationManager=null;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Toast.makeText(this, "Changed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}

