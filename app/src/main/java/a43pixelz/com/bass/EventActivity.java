package a43pixelz.com.bass;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class EventActivity extends AppCompatActivity {
    FloatingActionButton addeventbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        addeventbtn=findViewById(R.id.addeventbtn);
        addeventbtn.setOnClickListener(v->{
            startActivity(new Intent(this,AddEventActivity.class));
        });
    }
}
