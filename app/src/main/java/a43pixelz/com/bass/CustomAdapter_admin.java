package a43pixelz.com.bass;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import a43pixelz.com.bass.pojo.Admin.Admin;
import a43pixelz.com.bass.pojo.Admin.AdminBody;
import a43pixelz.com.bass.pojo.Entity.Body;
import a43pixelz.com.bass.pojo.Entity.Entity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nirmal on 9/22/2018.
 */

public class CustomAdapter_admin extends ArrayAdapter<ListDataModel_admin> implements View.OnClickListener{

    private ArrayList<ListDataModel_admin> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtType;
        TextView info;
        ImageView edit;
        ImageView image;
    }
    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
    public CustomAdapter_admin(ArrayList<ListDataModel_admin> data, Context context) {
        super(context, R.layout.listview_row_item_admin, data);
        this.dataSet = data;
        this.mContext=context;

    }
    AlertDialog alert;
    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        ListDataModel_admin dataModel=(ListDataModel_admin)object;

        LayoutInflater li = LayoutInflater.from(v.getRootView().getContext());
        View promptsView2 = li.inflate(R.layout.admin_popup,null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(v.getRootView().getContext());
        alertDialogBuilder.setView(promptsView2);

        alert = alertDialogBuilder.create();
        alert.setCancelable(true);
        switch (v.getId())
        {
            case R.id.item_info:
                SPref sPref=new SPref(v.getRootView().getContext());
                sPref.setValue("temp_admin_id",dataModel.getAdminid());
                alert.show();
                alertview(v.getRootView().getContext());
                break;
            case R.id.item_edit:
                Intent entityUpdate=new Intent(v.getRootView().getContext(),AddAdminActivity.class);
                entityUpdate.putExtra("admin_id", dataModel.getAdminid());
                entityUpdate.putExtra("entity_id", dataModel.getId());
                v.getRootView().getContext().startActivity(entityUpdate);
                break;
        }
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
         // Get the data item for this position
        ListDataModel_admin dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.listview_row_item_admin, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.txtType = (TextView) convertView.findViewById(R.id.type);
            viewHolder.info = (TextView) convertView.findViewById(R.id.item_info);
            viewHolder.edit = (ImageView) convertView.findViewById(R.id.item_edit);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.admin_image);
            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtName.setText(dataModel.getName());
        viewHolder.txtType.setText(dataModel.getId());
        viewHolder.image.setImageResource(R.drawable.user);
        viewHolder.info.setOnClickListener(this);
        viewHolder.info.setTag(position);
        viewHolder.edit.setOnClickListener(this);
        viewHolder.edit.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }
    private void alertview(Context context) {
        SPref sPref=new SPref(context);
        try{
        Call<Admin> call = apiInterface.getAdminById(sPref.getValue("temp_admin_id"));
        call.enqueue(new Callback<Admin>() {
            @Override
            public void onResponse(Call<Admin> call, Response<Admin> response) {
                AdminBody e = response.body().getBody().get(0);
                TextView popup_entity_id=alert.findViewById(R.id.popup_admin_id);
                popup_entity_id.setText(e.getId());
                TextView popup_entity_name=alert.findViewById(R.id.popup_admin_name);
                popup_entity_name.setText(e.getName());
                TextView popup_entity_address=alert.findViewById(R.id.popup_admin_mail);
                popup_entity_address.setText(e.getEmail());
                TextView popup_entity_location=alert.findViewById(R.id.popup_admin_phone);
                popup_entity_location.setText(e.getPhone());
                TextView popup_entity_entity=alert.findViewById(R.id.popup_admin_entity);
                popup_entity_entity.setText(e.getEntity());
            }

            @Override
            public void onFailure(Call<Admin> call, Throwable t) {

            }
        });}catch (Exception exe){

        }
    }
}
