package a43pixelz.com.bass;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import a43pixelz.com.bass.pojo.Admin.Admin;
import a43pixelz.com.bass.pojo.Admin.AdminBody;
import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Entity_Activity extends AppCompatActivity {
APIInterface apiInterface;
ListView listView;
    ListView listView2;
    ArrayList<ListDataModel_admin> dataModels;
    ArrayList<ListDataModel_user> dataModels2;
    Intent adminView;
    Intent userUpdate;
    private static CustomAdapter_admin adapter;
    private static CustomAdapter_user adapter2;
    String entity_ID="";
    String entity_Name="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entity_);
        Bundle extras = getIntent().getExtras();

        if(extras != null){
            entity_ID=extras.getString("entity_id");
            entity_Name=extras.getString("entity_name");
        }
        getSupportActionBar().setTitle(entity_Name);
        getSupportActionBar().setSubtitle(entity_ID);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        adminView = new Intent(this, AddAdminActivity.class);

        userUpdate = new Intent(this, CalenderViewActivity.class);
        listView=(ListView)findViewById(R.id.entity_admin_list);
        listView2=(ListView)findViewById(R.id.entity_user_list);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        try{
        Call<Admin> call = apiInterface.doGetListAdmin();
        call.enqueue(new Callback<Admin>() {
        @Override
        public void onResponse(Call<Admin> call, Response<Admin> response) {
            Log.d("TAG",response.code()+"");
            dataModels = new ArrayList<>();
            String displayResponse = "";
            Admin resource = response.body();
            Integer count = resource.getCount();
            List<AdminBody> datumList = resource.getBody();


            for (AdminBody datum : datumList) {
                if (datum.getEntity().matches(entity_ID)) {
                    String name = datum.getName();
                    String id = datum.getUserid();
                    dataModels.add(new ListDataModel_admin(name, id, "", ""));
                }
            }

            adapter= new CustomAdapter_admin(dataModels,getApplicationContext());

            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    ListDataModel_admin dataModel= dataModels.get(position);
                    adminView.putExtra("admin_id", dataModel.getId());
                    adminView.putExtra("entity_id", dataModel.getId());
                    adminView.putExtra("editable", false);
                    startActivity(adminView);
//                        Snackbar.make(view, dataModel.getName()+"\n"+dataModel.getId()+" API: "+dataModel.getVersion_number(), Snackbar.LENGTH_LONG)
//                                .setAction("No action", null).show();
                }
            });

        }

        @Override
        public void onFailure(Call<Admin> call, Throwable t) {
            call.cancel();
        }
    });}catch (Exception exe){

        }
try{
        Call<User> call2 = apiInterface.doGetListUser();
        call2.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call2, Response<User> response) {
                Log.d("TAG",response.code()+"");
                dataModels2 = new ArrayList<>();
                String displayResponse = "";
                User resource = response.body();
                Integer count = resource.getCount();
                List<UserBody> datumList = resource.getBody();


                for (UserBody datum : datumList) {
                    if(datum.getEntity().matches(entity_ID)){
                        String name = datum.getName();
                        String id = datum.getUserid();
                        dataModels2.add(new ListDataModel_user(name, id, "", ""));
                    }
                }

                adapter2= new CustomAdapter_user(dataModels2,getApplicationContext());

                listView2.setAdapter(adapter2);
                adapter2.notifyDataSetChanged();
                listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        ListDataModel_user dataModel= dataModels2.get(position);
                        userUpdate.putExtra("user_id", dataModel.getId());
                        startActivity(userUpdate);
                    }
                });

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                call.cancel();
                Toast.makeText(Entity_Activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });}catch (Exception exe){

}
     }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
