package a43pixelz.com.bass;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import a43pixelz.com.bass.pojo.Atendance.Attendance;
import a43pixelz.com.bass.pojo.Entity.Body;
import a43pixelz.com.bass.pojo.Entity.Entity;
import a43pixelz.com.bass.pojo.Shift.Shift;
import a43pixelz.com.bass.pojo.Shift.ShiftBody;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    APIInterface apiInterface;
    ArrayList<ListDataModel_entity> dataModels;
    ListView listView;
    Intent entityUpdate;
    ProgressDialog dialog;
    private static CustomAdapter_entity adapter;

    SPref sPref;

    @Override
    protected void onResume() {


        try {
            Call<Entity> call = apiInterface.doGetListResources();
            call.enqueue(new Callback<Entity>() {
                @Override
                public void onResponse(Call<Entity> call, Response<Entity> response) {
                    Log.d("TAG", response.code() + "");
                    dataModels = new ArrayList<>();
                    Entity resource = response.body();
                    if (resource != null) {
                        List<Body> datumList = resource.getBody();

                        for (Body datum : datumList) {
                            String name = datum.getName();
                            String id = datum.getEntityid();
                            dataModels.add(new ListDataModel_entity(name, id, "", ""));
                        }

                        adapter = new CustomAdapter_entity(dataModels, getApplicationContext());

                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                ListDataModel_entity dataModel = dataModels.get(position);

//                        Snackbar.make(view, dataModel.getName()+"\n"+dataModel.getId()+" API: "+dataModel.getVersion_number(), Snackbar.LENGTH_LONG)
//                                .setAction("No action", null).show();

                                entityUpdate.putExtra("entity_id", dataModel.getId());
                                entityUpdate.putExtra("entity_name", dataModel.getName());
                                startActivity(entityUpdate);

                            }
                        });

                    }

                }

                @Override
                public void onFailure(Call<Entity> call, Throwable t) {
                    call.cancel();
                }
            });
        } catch (Exception exe) {

        }
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sPref=new SPref(MainActivity.this);

        dialog =new ProgressDialog(MainActivity.this);
        dialog.setMessage("Loading Please wait");
        entityUpdate = new Intent(this, Entity_Activity.class);
        listView=(ListView)findViewById(R.id.entity_list);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);




        /**
         GET List Resources
         **/

        //

//        /**
//         Create new user
//         **/
//        Body entity = new Body();
//        entity.setEntityid("radomid");
//        entity.setName("app");
//        entity.setAddress("addd");
//        entity.setLocation("loc");
//
//        Call<Body> call1 = apiInterface.createEntity(entity);
//        call1.enqueue(new Callback<Body>() {
//            @Override
//            public void onResponse(Call<Body> call, Response<Body> response) {
//                Body user1 = response.body();
//
//                Toast.makeText(getApplicationContext(), response.code()+"" , Toast.LENGTH_SHORT).show();
//
//            }
//
//            @Override
//            public void onFailure(Call<Body> call, Throwable t) {
//                call.cancel();
//            }
//        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_entity) {
            startActivity(new Intent(this, AddEntityActivity.class));
            return true;
        }
        else if (id == R.id.action_logout) {
            sPref.deleteSpref();
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_entities) {
            // Handle the camera action
        } else if (id == R.id.nav_add_admins) {
            startActivity(new Intent(this, AddAdminActivity.class));
        } else if (id == R.id.nav_admins) {
            startActivity(new Intent(this, AdminListActivity.class));
        } else if (id == R.id.nav_users) {
            startActivity(new Intent(this, UserListActivity.class));
        }else if (id == R.id.notification) {
            startActivity(new Intent(this, NotificationActivity.class));
        }else if (id == R.id.attendancelist){
            startActivity(new Intent(this, AttendanceListActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
