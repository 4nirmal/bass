package a43pixelz.com.bass;

/**
 * Created by Nirmal on 9/22/2018.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class APIClient {

    private static Retrofit retrofit = null;

    static Retrofit getClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

      /*  Gson gson = new GsonBuilder()
                .setLenient()
                .create();*/

        retrofit = new Retrofit.Builder()
   //             .baseUrl("http://192.168.43.104/bassapi/")
//                .baseUrl("https://a4347268.ngrok.io/PhpProject1/")
               .baseUrl("http://ts.basspris.com/")
                //.baseUrl("http://192.168.43.174/basspris/")
//                .baseUrl("http://153.59.158.89/PhpProject1/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();


        return retrofit;
    }
}