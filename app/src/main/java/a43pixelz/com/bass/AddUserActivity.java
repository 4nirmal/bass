package a43pixelz.com.bass;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import a43pixelz.com.bass.pojo.Entity.Body;
import a43pixelz.com.bass.pojo.Entity.Entity;
import a43pixelz.com.bass.pojo.Mailer.SendMail;
import a43pixelz.com.bass.pojo.Shift.Shift;
import a43pixelz.com.bass.pojo.Shift.ShiftBody;
import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddUserActivity extends AppCompatActivity {

    EditText uname, uuserid, uemail, upassword, udesc,  ulocation;
    EditText phone,macaddress;
    Button add, update;
    APIInterface apiInterface;
    String userid;
    String entityid;
    Spinner uentity,ushift;
    RadioButton male,female,other;
    ArrayAdapter<String> adapt;
    ArrayAdapter<String> adapt2;
    ArrayList<String> entities;
    ArrayList<String> shifts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SPref sPref=new SPref(this);
        uname = findViewById(R.id.user_name_txt);
        uuserid = findViewById(R.id.user_id_txt);
        uemail = findViewById(R.id.user_email_txt);
        upassword = findViewById(R.id.user_password_txt);
        udesc = findViewById(R.id.user_desc_txt);
        uentity = findViewById(R.id.user_entity_txt);
        ulocation = findViewById(R.id.user_location_txt);
        ushift = findViewById(R.id.user_shift);
        add = findViewById(R.id.user_add_btn);
        update = findViewById(R.id.user_update_btn);
        phone=findViewById(R.id.user_phone);
        macaddress=findViewById(R.id.user_mac_address);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        male=findViewById(R.id.radio_male);
        female=findViewById(R.id.radio_female);
        other=findViewById(R.id.radio_other);
        //load spinner
        male.setOnClickListener(this::onRadioButtonClicked);
        female.setOnClickListener(this::onRadioButtonClicked);
        other.setOnClickListener(this::onRadioButtonClicked);

        entities=new ArrayList<String>();
        adapt=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,entities);
        adapt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        uentity.setAdapter(adapt);

        shifts=new ArrayList<String>();
        adapt2=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,shifts);
        adapt2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ushift.setAdapter(adapt2);

//        try{
//        Call<Entity> call = apiInterface.doGetListResources();
//        call.enqueue(new Callback<Entity>() {
//            @Override
//            public void onResponse(Call<Entity> call, Response<Entity> response) {
//                Entity resource = response.body();
//                List<Body> datumList = resource.getBody();
//                for (Body datum : datumList) {
//
//                }
//            }
//            @Override
//            public void onFailure(Call<Entity> call, Throwable t) {
//                call.cancel();
//            }
//        });
//    }catch (Exception exe){
//
//    }
        entities.add(sPref.getValue("admin_entity"));
        adapt.notifyDataSetChanged();
        /*try{
>>>>>>> Stashed changes
        Call<Entity> call = apiInterface.doGetListResources();
        call.enqueue(new Callback<Entity>() {
            @Override
            public void onResponse(Call<Entity> call, Response<Entity> response) {
                Entity resource = response.body();
                List<Body> datumList = resource.getBody();
                for (Body datum : datumList) {
                    entities.add(datum.getEntityid());
                    adapt.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<Entity> call, Throwable t) {
                call.cancel();
            }
        });
    }catch (Exception exe){

<<<<<<< Updated upstream
    }
=======
    }*/
try{
        Call<Shift> call2 = apiInterface.doGetListShift();
        call2.enqueue(new Callback<Shift>() {
            @Override
            public void onResponse(Call<Shift> call2, Response<Shift> response) {
                Shift resource = response.body();
                List<ShiftBody> datumList = resource.getBody();
                for (ShiftBody datum : datumList) {
                    shifts.add(datum.getShiftFrom()+" - "+datum.getShiftTo());
                    adapt2.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<Shift> call2, Throwable t) {
                call2.cancel();
            }
        });
        }catch (Exception exe){

    }

        //

        Bundle extras = getIntent().getExtras();
        if(extras!=null){
        userid = extras.getString("user_id");
        if(!userid.isEmpty()){
            getSupportActionBar().setTitle(userid);
            update.setEnabled(true);
            add.setEnabled(false);
            uuserid.setText(userid);
            add.setVisibility(View.GONE);
            loadUserDetails();
        }
        }else{
            macaddress.setEnabled(false);
            getSupportActionBar().setTitle("Add User");
        add.setVisibility(View.VISIBLE);
        update.setVisibility(View.GONE);
        upassword.setText(random(8));

        }

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = uname.getText().toString();
                String email = uemail.getText().toString();
                String pass = upassword.getText().toString();
                String desc = udesc.getText().toString();
                String entity = uentity.getSelectedItem().toString();
                String loc = ulocation.getText().toString();
                String phonee = phone.getText().toString();
                String macaddresss=macaddress.getText().toString();
                String shift=ushift.getSelectedItem().toString();
                String gender="";
                if(male.isChecked()){
                    gender="male";
                }else if(female.isChecked()){
                    gender="female";
                }else if (other.isChecked()){
                    gender="other";
                }
                if(name.matches("")&&email.matches("")&&pass.matches("")&&desc.matches("")&&entity.matches("")&&loc.matches("")&&phonee.matches("")&&!macaddresss.matches("")&&gender.matches("")){
                    Toast.makeText(AddUserActivity.this, "Plese Fill all details", Toast.LENGTH_SHORT).show();
                }else{
                    UserBody u = new UserBody();
                    u.setName(name);
                    u.setEmail(email);
                    u.setPassword(pass);
                    u.setDescription(desc);
                    u.setShift(shift);
                    u.setEntity(entity);
                    u.setLocation(loc);
                    u.setPhone(phonee);
                    u.setMac_address("");
                    u.setGender(gender);
                    try{
                    Call<UserBody> call = apiInterface.createUser(u);
                    call.enqueue(new Callback<UserBody>() {
                        @Override
                        public void onResponse(Call<UserBody> call, Response<UserBody> response) {
                            Toast.makeText(getApplicationContext(), "User Added", Toast.LENGTH_SHORT).show();
                            String wmessage="<h3>Hi "+name+",</h3><h2 style='float:center;'>WELCOME</h2><hr><br> <p>Your profile has been created for this mail id "+email+".</p><br> Your password is "+pass +" .</p><br><br><p>Thanks,</p><p>BASS</p>";
                            SendMail sm = new SendMail(AddUserActivity.this, email, "Your profile created", wmessage);
                            sm.execute();
                            finish();
                        }

                        @Override
                        public void onFailure(Call<UserBody> call, Throwable t) {

                        }
                    });}catch (Exception exe){

                    }
                }


            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = uname.getText().toString();
                String email = uemail.getText().toString();
                String pass = upassword.getText().toString();
                String desc = udesc.getText().toString();
                String entity = uentity.getSelectedItem().toString();
                String loc = ulocation.getText().toString();
                String phonee = phone.getText().toString();
                String macaddresss=macaddress.getText().toString();
                String shift = ushift.getSelectedItem().toString();
                String gender="";
                if(male.isChecked()){
                    gender="male";
                }else if(female.isChecked()){
                    gender="female";
                }else if (other.isChecked()){
                    gender="other";
                }
                if(name.matches("")&&email.matches("")&&pass.matches("")&&desc.matches("")&&entity.matches("")&&loc.matches("")&&phonee.matches("")&&gender.matches("")){
                    Toast.makeText(AddUserActivity.this, "Plese Fill all details", Toast.LENGTH_SHORT).show();
                }else{
                UserBody u = new UserBody();
                u.setUserid(userid);
                    u.setName(name);u.setEmail(email);u.setPassword(pass);u.setDescription(desc);u.setShift(shift);
                    u.setEntity(entity);u.setLocation(loc);u.setPhone(phonee);u.setMac_address(macaddresss);u.setGender(gender);
                    try{
                    Call<UserBody> call = apiInterface.updateUser(u);
                call.enqueue(new Callback<UserBody>() {
                    @Override
                    public void onResponse(Call<UserBody> call, Response<UserBody> response) {
                        Toast.makeText(getApplicationContext(), response.code()+"", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<UserBody> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_LONG).show();
                    }
                });
                    }catch (Exception exe){

                    }
            }
            }
        });
    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.radio_male:
                female.setChecked(false);
                other.setChecked(false);
                break;
            case R.id.radio_female:
                male.setChecked(false);
                other.setChecked(false);
                break;
            case R.id.radio_other:
                female.setChecked(false);
                male.setChecked(false);
                break;
        }
    }
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
    private static final String ALLOWED_CHARACTERS ="0123456789QWERTYUIOPASDFGHJKLZXCVBNM!@#$%*()qwertyuiopasdfghjklzxcvbnm";

    private static String random(final int sizeOfRandomString)
    {
        final Random random=new Random();
        final StringBuilder sb=new StringBuilder(sizeOfRandomString);
        for(int i=0;i<sizeOfRandomString;++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }

    private void loadUserDetails(){
        try{
        Call<User> call = apiInterface.getUserById(userid);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                UserBody data = response.body().getBody().get(0);
                uname.setText(data.getName());
                uemail.setText(data.getEmail());
                upassword.setText(data.getPassword());
                udesc.setText(data.getDescription());
                uentity.setSelection(adapt.getPosition(data.getEntity()));
                ushift.setSelection(adapt2.getPosition(data.getEntity()));
                ulocation.setText(data.getLocation());
                switch (data.getGender()){
                    case "male":
                        male.setChecked(true);
                        break;
                    case "female":
                        female.setChecked(true);
                        break;
                    case "other":
                        other.setChecked(true);
                        break;
                }
                phone.setText(data.getPhone());
                macaddress.setText(data.getMac_address());
               // Toast.makeText(AddUserActivity.this,  String.valueOf(adapt.getPosition(data.getEntity())), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });}catch (Exception exe){

    }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
