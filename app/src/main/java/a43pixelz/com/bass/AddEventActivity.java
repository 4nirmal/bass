package a43pixelz.com.bass;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import a43pixelz.com.bass.pojo.Notification.NotificationBody;
import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddEventActivity extends AppCompatActivity {
    //Declaration of Google API Client

    int PLACE_PICKER_REQUEST = 1;

    Button datebtn,timebtn,location;
    FloatingActionButton addevent;
    EditText description;
    EditText summary;
    Calendar myCalendar;
    APIInterface apiInterface;
    SPref sPref;
    MultiSelectDialog multiSelectDialog;

    //In the onCreate method of the Activity initialize the GoogleApiClient.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        datebtn=findViewById(R.id.datebtn);
        timebtn=findViewById(R.id.timebtn);
        location=findViewById(R.id.locationbtn);
        addevent=findViewById(R.id.addeventbtn);
        summary=findViewById(R.id.summary);
        description=findViewById(R.id.description);
        sPref=new SPref(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        location.setOnClickListener(v->{

            try {
              //  Places.initialize(getApplicationContext(), "AIzaSyAJVD7Zal_PEbj3g7putt-aLUQpRRSHkMg");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, (timePicker, selectedHour, selectedMinute) -> timebtn.setText( selectedHour + ":" + selectedMinute), myCalendar.get(Calendar.HOUR), myCalendar.get(Calendar.MINUTE), false);
        datebtn.setOnClickListener(v->
            new DatePickerDialog(AddEventActivity.this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show());
        timebtn.setOnClickListener(v-> mTimePicker.show());
        addevent.setOnClickListener(v->{
            Event event = new Event()
                    .setSummary("Google I/O 2015")
                    .setLocation("800 Howard St., San Francisco, CA 94103")
                    .setDescription("A chance to hear more about Google's developer products.");

            DateTime startDateTime = new DateTime("2015-05-28T09:00:00-07:00");
            EventDateTime start = new EventDateTime()
                    .setDateTime(startDateTime)
                    .setTimeZone("America/Los_Angeles");
            event.setStart(start);

            DateTime endDateTime = new DateTime("2015-05-28T17:00:00-07:00");
            EventDateTime end = new EventDateTime()
                    .setDateTime(endDateTime)
                    .setTimeZone("America/Los_Angeles");
            event.setEnd(end);

            String[] recurrence = new String[] {"RRULE:FREQ=DAILY;COUNT=2"};
            event.setRecurrence(Arrays.asList(recurrence));

            EventAttendee[] attendees = new EventAttendee[] {
                    new EventAttendee().setEmail("lpage@example.com"),
                    new EventAttendee().setEmail("sbrin@example.com"),
            };
            event.setAttendees(Arrays.asList(attendees));

            EventReminder[] reminderOverrides = new EventReminder[] {
                    new EventReminder().setMethod("email").setMinutes(24 * 60),
                    new EventReminder().setMethod("popup").setMinutes(10),
            };
            Event.Reminders reminders = new Event.Reminders()
                    .setUseDefault(false)
                    .setOverrides(Arrays.asList(reminderOverrides));
            event.setReminders(reminders);

            String calendarId = "primary";

           //event = service.events().insert(calendarId, event).execute();
            System.out.printf("Event created: %s\n", event.getHtmlLink());
            NotificationBody n=new NotificationBody();
            n.setType("User");
            n.setNotificationTitle(summary.getText().toString());
            n.setNotificationMessage(description.getText().toString());
            //todo :Receiver ID To be set
            //n.setReceiverId(spinner.getSelectedItem().toString());
            try{
                Call<NotificationBody> call = apiInterface.createNotification(n);
                call.enqueue(new Callback<NotificationBody>() {
                    @Override
                    public void onResponse(Call<NotificationBody> call, Response<NotificationBody> response) {
                        Toast.makeText(getApplicationContext(), "Message sent", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<NotificationBody> call, Throwable t) {
                        call.cancel();
                    }
                });
            }catch (Exception exe){

            }
        });
        ArrayList<Integer> alreadySelectedCountries=new ArrayList<>();
        //MultiSelectModel

        ArrayList<com.abdeveloper.library.MultiSelectModel> listOfCountries=new ArrayList<>();
        Call<User> userCall = apiInterface.doGetListUser();
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User resource = response.body();
                List<UserBody> datumList = resource.getBody();
                int c=0;
                for (UserBody datum : datumList) {
                    if(datum.getEntity().contains(sPref.getValue(sPref.getKey("entity")))){
                        MultiSelectModel e=new MultiSelectModel(c,datum.getName());
                        listOfCountries.add(e);
                    }
                    multiSelectDialog = new MultiSelectDialog()
                            .title("User List") //setting title for dialog
                            .titleSize(25)
                            .positiveText("Done")
                            .negativeText("Cancel")
                            .setMinSelectionLimit(1) //you can set minimum checkbox selection limit (Optional)
                            .setMaxSelectionLimit(listOfCountries.size()) //you can set maximum checkbox selection limit (Optional)
                            .preSelectIDsList(alreadySelectedCountries) //List of ids that you need to be selected
                            .multiSelectList(listOfCountries) // the multi select model list with ids and name
                            .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                                @Override
                                public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                                    //will return list of selected IDS
                                    //alreadySelectedCountries.clear();
                                    for (int i = 0; i < selectedIds.size(); i++) {
                                        //alreadySelectedCountries.add(selectedIds.get(i));
                                        Toast.makeText(AddEventActivity.this, "Selected Ids : " + selectedIds.get(i) + "\n" +
                                                "Selected Names : " + selectedNames.get(i) + "\n" +
                                                "DataString : " + dataString, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onCancel() {
                                    Log.d("Tag", "Dialog cancelled");
                                }
                            });
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                call.cancel();
            }
        });
        Button button=findViewById(R.id.button);


        button.setOnClickListener(v->{if(multiSelectDialog!=null)multiSelectDialog.show(getSupportFragmentManager(),"");});

    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        datebtn.setText(sdf.format(myCalendar.getTime()));
    }




}
