package a43pixelz.com.bass;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nirmal on 12/25/2018.
 */

public class CustomAdapter_attendance extends ArrayAdapter<ListDataModal_attendance> implements View.OnClickListener{

    private ArrayList<ListDataModal_attendance> dataSet;
    Context mContext;
    ArrayList<ListDataModel_user> users;

    // View lookup cache
    private static class ViewHolder {
//        ImageView icon;
        TextView txtName;
        TextView txtInfo;
        ImageView inLoc;
        ImageView outLoc;
        ImageView usericon;
        ImageView usericon2;
        TextView txtInfo2;
    }

    public CustomAdapter_attendance(ArrayList<ListDataModal_attendance> data, ArrayList<ListDataModel_user> userdatamodals, Context context) {
        super(context, R.layout.listview_row_item_attendance, data);
        this.dataSet = data;
        this.users = userdatamodals;
        this.mContext=context;
    }
    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ListDataModal_attendance dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        a43pixelz.com.bass.CustomAdapter_attendance.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.listview_row_item_attendance, parent, false);
//            viewHolder.icon = (ImageView) convertView.findViewById(R.id.admin_image);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.usericon=convertView.findViewById(R.id.admin_image);
            viewHolder.usericon2=convertView.findViewById(R.id.admin_image2);
            viewHolder.txtInfo = (TextView) convertView.findViewById(R.id.att_info);
            viewHolder.outLoc = (ImageView) convertView.findViewById(R.id.att_outloc);
            viewHolder.inLoc = (ImageView) convertView.findViewById(R.id.att_inloc);
            viewHolder.txtInfo2 = (TextView) convertView.findViewById(R.id.att_info2);
            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (a43pixelz.com.bass.CustomAdapter_attendance.ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;
        String name = "N/A";
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId().contains(dataModel.getUserid())) {
                name = users.get(i).getName();
            }
        }
        @SuppressLint("SimpleDateFormat")
        String strDate = new SimpleDateFormat("HH:mm").format(new Date(Long.parseLong(dataModel.getIntime())));
        viewHolder.txtName.setText(name);
        String info = " IN- " + strDate;
        String strDate2;
        if(dataModel.getOuttime().matches("")){
             strDate2 = "Nil";
        }else{
            strDate2 = new SimpleDateFormat("HH:mm").format(new Date(Long.parseLong(dataModel.getOuttime())));
        }
        String info2=" OUT- "+ strDate2;
        viewHolder.txtInfo.setText(info);
        viewHolder.txtInfo2.setText(info2);
        viewHolder.usericon.setImageResource(R.drawable.usericon);
        viewHolder.usericon2.setImageResource(R.drawable.usericon);
        viewHolder.inLoc.setOnClickListener(this);
        viewHolder.inLoc.setTag(position);
        if(dataModel.getOuttime().matches("")){
            viewHolder.outLoc.setImageResource(R.drawable.disabled);
        }else{
            viewHolder.outLoc.setOnClickListener(this);
            viewHolder.outLoc.setTag(position);
        }


        // Return the completed view to render on screen
        return convertView;
    }

    @Override
    public void onClick(View v) {
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        ListDataModal_attendance dataModel=(ListDataModal_attendance)object;
        switch (v.getId())
        {
            case R.id.att_inloc:
                Uri mapUri = Uri.parse("geo:0,0?q="+ dataModel.getInloc());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                v.getContext().startActivity(mapIntent);
//                Toast.makeText(v.getContext(), dataModel.getInloc(), Toast.LENGTH_LONG).show();
                break;
            case R.id.att_outloc:
                Uri mapUri2 = Uri.parse("geo:0,0?q="+ dataModel.getInloc());
                Intent mapIntent2 = new Intent(Intent.ACTION_VIEW, mapUri2);
                mapIntent2.setPackage("com.google.android.apps.maps");
                v.getContext().startActivity(mapIntent2);
//                Toast.makeText(v.getContext(), dataModel.getOutloc(), Toast.LENGTH_LONG).show();
                break;
        }
    }

 }

