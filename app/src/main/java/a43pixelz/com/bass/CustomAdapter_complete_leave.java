package a43pixelz.com.bass;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import a43pixelz.com.bass.pojo.Leave.Leave;
import a43pixelz.com.bass.pojo.Leave.LeaveBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nirmal on 9/22/2018.
 */

public class CustomAdapter_complete_leave extends ArrayAdapter<ListDataModel_complete_leave> implements View.OnClickListener{

    private ArrayList<ListDataModel_complete_leave> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView Uname;
        TextView LeaveDays;

        ImageView image;

        TextView LeaveReason;
        TextView Leaveupdatedby;
        TextView comment;
    }
    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
    public CustomAdapter_complete_leave(ArrayList<ListDataModel_complete_leave> data, Context context) {
        super(context, R.layout.completed_leave, data);
        this.dataSet = data;
        this.mContext=context;

    }
    AlertDialog alert;
    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);

        final android.support.v7.app.AlertDialog.Builder adb = new android.support.v7.app.AlertDialog.Builder(v.getRootView().getContext());
        adb.setTitle("Warning");
        adb.setMessage("Are you sure to provide leave?");
        adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            } });
        adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //ca
            } });
        adb.setCancelable(false);
        switch (v.getId())
        {
            case R.id.approve_leave_request:
                adb.show();
                break;
            case R.id.reject_leave_request:

                break;
        }
    }




    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
         // Get the data item for this position
        ListDataModel_complete_leave dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.completed_leave, parent, false);
            viewHolder.image =(ImageView) convertView.findViewById(R.id.user_leave_image);
            viewHolder.LeaveReason =(TextView) convertView.findViewById(R.id.leave_request_reason);
            viewHolder.LeaveDays =(TextView) convertView.findViewById(R.id.leave_request_date);
            viewHolder.Uname =(TextView) convertView.findViewById(R.id.leave_request_name);
            viewHolder.Leaveupdatedby =(TextView) convertView.findViewById(R.id.leave_status);
            viewHolder.comment =(TextView) convertView.findViewById(R.id.leave_comment);
            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.image.setImageResource(R.drawable.user);
        viewHolder.LeaveReason.setText(dataModel.getleaveReason());
        viewHolder.LeaveDays.setText(dataModel.getleavedate());
        viewHolder.Uname.setText(dataModel.getName());
        viewHolder.Leaveupdatedby.setText(dataModel.getupdatedby());
        viewHolder.comment.setText(dataModel.getcomment());
        // Return the completed view to render on screen
        return convertView;
    }
    private void alertview(Context context) {
        SPref sPref=new SPref(context);
        try{
        Call<Leave> call = apiInterface.getLeaveById(sPref.getValue("temp_leave_id"));
        call.enqueue(new Callback<Leave>() {
            @Override
            public void onResponse(Call<Leave> call, Response<Leave> response) {
                LeaveBody e = response.body().getBody().get(0);
                TextView popup_entity_id=alert.findViewById(R.id.popup_user_id);
                popup_entity_id.setText(e.getEntityid());

            }

            @Override
            public void onFailure(Call<Leave> call, Throwable t) {

            }
        });}catch (Exception exe){

        }
    }
}
