package a43pixelz.com.bass;

/**
 * Created by Nirmal on 12/25/2018.
 */

public class ListDataModal_attendance {
    String name;
    String intime;
    String outtime;
    String inloc;
    String outloc;
    String userid;

    public ListDataModal_attendance(String name, String intime, String outtime, String inloc, String outloc, String userid) {
        this.name=name;
        this.intime = intime;
        this.outtime = outtime;
        this.inloc = inloc;
        this.outloc = outloc;
        this.userid = userid;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntime() {
        return intime;
    }

    public void setIntime(String intime) {
        this.intime = intime;
    }

    public String getOuttime() {
        return outtime;
    }

    public void setOuttime(String outtime) {
        this.outtime = outtime;
    }

    public String getInloc() {
        return inloc;
    }

    public void setInloc(String inloc) {
        this.inloc = inloc;
    }

    public String getOutloc() {
        return outloc;
    }

    public void setOutloc(String outloc) {
        this.outloc = outloc;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
