package a43pixelz.com.bass;

/**
 * Created by Nirmal on 9/22/2018.
 */

public class ListDataModel_request_leave {

    String name;
    String leave_id;
    String leave_Date;
    String leave_reason;
    int image;

    public ListDataModel_request_leave(String name, String leave_id, String leave_Date, String leave_reason ,int image) {
        this.name=name;
        this.leave_id=leave_id;
        this.leave_Date=leave_Date;
        this.leave_reason=leave_reason;
        this.image=image;
    }

    public String getName() {
        return name;
    }

    public String getLeaveId() {
        return leave_id;
    }

    public String getleavedate() {
        return leave_Date;
    }

    public String getleaveReason() {
        return leave_reason;
    }

    public int getUserImage() {
        return image;
    }
}