package a43pixelz.com.bass;

/**
 * Created by Nirmal on 9/22/2018.
 */

public class ListDataModel_complete_leave {

    String name;
    String leave_id;
    String leave_Date;
    String leave_reason;
    String updatedby;
    String comment;
    int image;

    public ListDataModel_complete_leave(String name, String leave_id, String leave_Date, String leave_reason ,int image,String updatedby,String comment) {
        this.name=name;
        this.leave_id=leave_id;
        this.updatedby=updatedby;
        this.comment=comment;
        this.image=image;
        this.leave_Date=leave_Date;
        this.leave_reason=leave_reason;
    }

    public String getName() {
        return name;
    }

    public String getLeaveId() {
        return leave_id;
    }

    public String getleavedate() {
        return leave_Date;
    }

    public String getleaveReason() {
        return leave_reason;
    }

    public int getUserImage() {
        return image;
    }

    public String getupdatedby() {
        return updatedby;
    }

    public String getcomment() {
        return comment;
    }
}