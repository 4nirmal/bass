package a43pixelz.com.bass;

/**
 * Created by Nirmal on 9/22/2018.
 */

import a43pixelz.com.bass.pojo.Admin.Admin;
import a43pixelz.com.bass.pojo.Admin.AdminBody;
import a43pixelz.com.bass.pojo.Atendance.Attendance;
import a43pixelz.com.bass.pojo.Atendance.AttendanceBody;
import a43pixelz.com.bass.pojo.Entity.Entity;
import a43pixelz.com.bass.pojo.Leave.Leave;
import a43pixelz.com.bass.pojo.Leave.LeaveBody;
import a43pixelz.com.bass.pojo.Notification.Notification;
import a43pixelz.com.bass.pojo.Notification.NotificationBody;
import a43pixelz.com.bass.pojo.Shift.Shift;
import a43pixelz.com.bass.pojo.Shift.ShiftBody;
import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

interface APIInterface {

    @GET("entity/read.php")
    Call<Entity> doGetListResources();

    @POST("entity/create.php")
    Call<a43pixelz.com.bass.pojo.Entity.Body> createEntity(@Body a43pixelz.com.bass.pojo.Entity.Body entity);

    @POST("entity/update.php")
    Call<a43pixelz.com.bass.pojo.Entity.Body> updateEntity(@Body a43pixelz.com.bass.pojo.Entity.Body entity);

    @GET("entity/get.php?")
    Call<Entity> getEntityById(@Query("id") String id);

    @GET("admin/read.php")
    Call<Admin> doGetListAdmin();

    @GET("admin/get.php?")
    Call<Admin> getAdminById(@Query("id") String id);

    @GET("admin/getlastnotify.php?")
    Call<Admin> getAdminnotifyById(@Query("id") String id);

    @POST("admin/create.php")
    Call<AdminBody> createAdmin(@Body AdminBody admin);

    @POST("admin/update.php")
    Call<AdminBody> updateAdmin(@Body AdminBody admin);

    @POST("admin/updatenotify.php")
    Call<AdminBody> updateAdminnotify(@Body AdminBody admin);

    @GET("user/read.php")
    Call<User> doGetListUser();

    @GET("user/get.php?")
    Call<User> getUserById(@Query("id") String id);

    @GET("user/getlastnotify.php?")
    Call<User> getUsernotifyById(@Query("id") String id);

    @POST("user/create.php")
    Call<UserBody> createUser(@Body UserBody user);

    @POST("user/update.php")
    Call<UserBody> updateUser(@Body UserBody user);

    @POST("user/updatenotify.php")
    Call<UserBody> updateUsernotify(@Body UserBody user);

    @GET("leave/read.php")
    Call<Leave> doGetListLeave();

    @GET("leave/get.php?")
    Call<Leave> getLeaveById(@Query("id") String id);

    @POST("leave/update.php")
    Call<LeaveBody> updateLeave(@Body LeaveBody leave);

    @GET("leave/getbyuser.php?")
    Call<Leave> getLeaveByUserId(@Query("id") String id);

    @POST("leave/create.php")
    Call<LeaveBody> createLeave(@Body LeaveBody leave);

    @GET("attendance/read.php")
    Call<Attendance> doGetListAttendance();

    @GET("attendance/get.php?")
    Call<Attendance> getAttendanceById(@Query("id") String id);

    @GET("attendance/getbyuser.php?")
    Call<Attendance> getAttendanceByUserId(@Query("id") String id);

    @GET("attendance/getbydate.php?")
    Call<Attendance> getAttendanceByDate(@Query("date") String Date);

    @POST("attendance/create.php")
    Call<AttendanceBody> createAttendance(@Body AttendanceBody leave);

    @POST("attendance/update.php")
    Call<AttendanceBody> updateAttendance(@Body AttendanceBody leave);

    @GET("admin/login.php?")
    Call<Admin> adminLogin(@Query("id") String id, @Query("password") String password);

    @GET("user/login.php?")
    Call<User> userLogin(@Query("id") String id, @Query("password") String password);

    @GET("shift/read.php")
    Call<Shift> doGetListShift();

    @POST("shift/create.php")
    Call<ShiftBody> createShift(@Body ShiftBody shift);

    @GET("shift/get.php?")
    Call<Shift> getShiftById(@Query("id") String id);


    @POST("notification/create.php")
    Call<NotificationBody> createNotification(@Body NotificationBody admin);

    @POST("notification/update.php")
    Call<AdminBody> updateNotification(@Body NotificationBody admin);

    @GET("notification/read.php")
    Call<Notification> doGetListNotification();

    @GET("notification/get.php?")
    Call<Notification> getNotificationById(@Query("id") String id);

    @GET("notification/getlastnotify.php?")
    Call<Notification> getNotificationBynotify(@Query("id") int id);
}