package a43pixelz.com.bass;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import a43pixelz.com.bass.pojo.Atendance.Attendance;
import a43pixelz.com.bass.pojo.Atendance.AttendanceBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalenderViewActivity extends AppCompatActivity {
    String userid;
    APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender_view);

        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            userid=extras.getString("User_id");
        }
        List<EventDay> events = new ArrayList<>();
        CalendarView calendarView = (CalendarView) findViewById(R.id.calendarView2);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        try{
        Call<Attendance> call = apiInterface.getAttendanceByUserId(userid);
        call.enqueue(new Callback<Attendance>() {
            @Override
            public void onResponse(Call<Attendance> call, Response<Attendance> response) {
                Log.d("TAG",response.code()+"");
                Attendance resource = response.body();
                List<AttendanceBody> datumList = resource.getBody();

                for (AttendanceBody datum : datumList) {
                    String dateStr = datum.getDatestr();

                    Date date = null;
                    try {
                        date = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(dateStr);
                    } catch (ParseException e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    events.add(new EventDay(cal, R.drawable.adduser));
                }
                calendarView.setEvents(events);

            }

            @Override
            public void onFailure(Call<Attendance> call, Throwable t) {

            }
        });}catch (Exception exe){

        }

    }
}
