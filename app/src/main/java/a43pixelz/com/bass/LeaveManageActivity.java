package a43pixelz.com.bass;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import a43pixelz.com.bass.pojo.Leave.Leave;
import a43pixelz.com.bass.pojo.Leave.LeaveBody;
import a43pixelz.com.bass.pojo.Leave.Leave;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeaveManageActivity extends AppCompatActivity {
APIInterface apiInterface;

    ArrayList<ListDataModel_request_leave> dataModels;
    ArrayList<ListDataModel_complete_leave> dataModels2;

    private static CustomAdapter_request_leave adapter;
    private static CustomAdapter_complete_leave adapter2;
    ListView listView;
    ListView listView2;
    SPref sPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_manage);

        sPref=new SPref(this);
        listView=(ListView)findViewById(R.id.pending_leave_request);
        listView2=(ListView)findViewById(R.id.completed_leave_request);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        try{
        Call<Leave> call = apiInterface.doGetListLeave();
        call.enqueue(new Callback<Leave>() {
            @Override
            public void onResponse(Call<Leave> call, Response<Leave> response) {
                Log.d("TAG",response.code()+"");
                dataModels = new ArrayList<>();
                dataModels2 = new ArrayList<>();
                String displayResponse = "";
                Leave resource = response.body();
                Integer count = resource.getCount();
                List<LeaveBody> datumList = resource.getBody();


                for (LeaveBody datum : datumList) {
                    if (datum.getStatus().matches("Requested")) {
                        if(datum.getEntityid().matches(sPref.getValue("admin_entity"))) {
                            String name = datum.getUserid();
                            String id = datum.getLeaveid();
                            String reason = datum.getReason();
                            String date = datum.getFrom() + " to " + datum.getTo();
                            dataModels.add(new ListDataModel_request_leave(name, id, date, reason, 0));
                        }
                    }
                    else{
                        if(datum.getEntityid().matches(sPref.getValue("admin_entity"))) {
                            String name = datum.getUserid();
                            String id = datum.getLeaveid();
                            String reason = datum.getReason();
                            String updatedby = datum.getStatus() + " by " + datum.getUpdatedBy();
                            String comment = datum.getComment();
                            String date2 = datum.getFrom() + " to " + datum.getTo();
                            dataModels2.add(new ListDataModel_complete_leave(name, id, date2, reason, 0, updatedby, comment));
                        }
                    }
                }

                adapter= new CustomAdapter_request_leave(dataModels,getApplicationContext());
                adapter2= new CustomAdapter_complete_leave(dataModels2,getApplicationContext());

                listView.setAdapter(adapter);
                listView2.setAdapter(adapter2);
                adapter.notifyDataSetChanged();
                adapter2.notifyDataSetChanged();
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        /*ListDataModel_request_leave dataModel= dataModels.get(position);
                        LeaveView.putExtra("Leave_id", dataModel.getLeaveId());
                        LeaveView.putExtra("user_id", dataModel.getName());
                        LeaveView.putExtra("editable", false);
                        startActivity(LeaveView);
//                        Snackbar.make(view, dataModel.getName()+"\n"+dataModel.getId()+" API: "+dataModel.getVersion_number(), Snackbar.LENGTH_LONG)
//                                .setAction("No action", null).show();*/
                    }
                });
            }
            @Override
            public void onFailure(Call<Leave> call, Throwable t) {
                call.cancel();
            }
        });}catch (Exception exe){

    }
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Leave Management");
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
