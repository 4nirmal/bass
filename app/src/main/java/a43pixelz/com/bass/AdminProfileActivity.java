package a43pixelz.com.bass;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import a43pixelz.com.bass.pojo.Admin.AdminBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminProfileActivity extends AppCompatActivity {
    TextView id,name,email,entity;
    EditText phone;
    EditText password;
    Button update;
    SPref sPref;
    APIInterface apiInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_profile);
        getSupportActionBar().setTitle("Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sPref=new SPref(this);
        id=findViewById(R.id.admin_id);
        name=findViewById(R.id.admin_name);
        email=findViewById(R.id.admin_email);
        entity=findViewById(R.id.admin_entity);
        password=findViewById(R.id.admin_password);
        update=findViewById(R.id.update_admin);
        phone=findViewById(R.id.admin_phone);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        id.setText(sPref.getValue("admin_id"));
        name.setText(sPref.getValue("admin_name"));
        email.setText(sPref.getValue("admin_email"));
        password.setText(sPref.getValue("admin_password"));
        entity.setText(sPref.getValue("admin_entity"));
        phone.setText(sPref.getValue("admin_phone"));
        update.setOnClickListener(v->{
            AdminBody u = new AdminBody();
            u.setName(sPref.getValue("admin_name"));
            u.setEmail(sPref.getValue("admin_email"));
            u.setPassword(password.getText().toString());
            u.setEntity(sPref.getValue("admin_location"));
            u.setLocation(sPref.getValue("admin_entity"));
            u.setPhone(phone.getText().toString());
            u.setUserid(sPref.getValue("admin_id"));
            try{
            Call<AdminBody> call = apiInterface.updateAdmin(u);
            call.enqueue(new Callback<AdminBody>() {
                @Override
                public void onResponse(Call<AdminBody> call, Response<AdminBody> response) {
                    Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(Call<AdminBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_LONG).show();
                }
            });}catch (Exception exe){

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
