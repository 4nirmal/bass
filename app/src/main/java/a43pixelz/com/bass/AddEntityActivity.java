package a43pixelz.com.bass;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import a43pixelz.com.bass.pojo.Entity.Body;
import a43pixelz.com.bass.pojo.Entity.Entity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddEntityActivity extends AppCompatActivity {

    EditText ename, eeid, eaddress, elocation;
    Button add, update;
    String entity_id;
    APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_entity);
        getSupportActionBar().setTitle("Add Entity");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ename = findViewById(R.id.entity_name_txt);
        eeid = findViewById(R.id.entity_id_txt);
        eaddress = findViewById(R.id.entity_address_txt);
        elocation = findViewById(R.id.entity_location_txt);

        add = findViewById(R.id.entity_add_btn);
        update = findViewById(R.id.entity_update_btn);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            entity_id = extras.getString("entity_id");
            getSupportActionBar().setTitle(entity_id);
            if(!entity_id.isEmpty()){
                update.setEnabled(true);
                add.setEnabled(false);
                eeid.setText(entity_id);
                add.setVisibility(View.GONE);
            loadentityDetails();
            }
        }else{
            add.setVisibility(View.VISIBLE);
            update.setVisibility(View.GONE);
        }

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = ename.getText().toString();
                String address = eaddress.getText().toString();
                String loc = elocation.getText().toString();
                Body e = new Body();
                e.setName(name);
                e.setLocation(loc);
                e.setAddress(address);
                try{
                Call<Body> call = apiInterface.createEntity(e);
                call.enqueue(new Callback<Body>() {
                    @Override
                    public void onResponse(Call<Body> call, Response<Body> response) {
                        Toast.makeText(getApplicationContext(), "Entity Added", Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<Body> call, Throwable t) {

                    }
                });        }catch (Exception exe){

                }
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = ename.getText().toString();
                String address = eaddress.getText().toString();
                String loc = elocation.getText().toString();
                Body e = new Body();
                e.setName(name);
                e.setLocation(loc);
                e.setAddress(address);
                e.setEntityid(entity_id);
                try{
                Call<Body> call = apiInterface.updateEntity(e);
                call.enqueue(new Callback<Body>() {
                    @Override
                    public void onResponse(Call<Body> call, Response<Body> response) {
                        Toast.makeText(getApplicationContext(), response.code()+"", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<Body> call, Throwable t) {

                    }
                });        }catch (Exception exe){

                }
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void loadentityDetails(){
        try{
        Call<Entity> call = apiInterface.getEntityById(entity_id);
        call.enqueue(new Callback<Entity>() {
            @Override
            public void onResponse(Call<Entity> call, Response<Entity> response) {
                Body e = response.body().getBody().get(0);
                ename.setText(e.getName());
                eaddress.setText(e.getAddress());
                elocation.setText(e.getLocation());
            }

            @Override
            public void onFailure(Call<Entity> call, Throwable t) {

            }
        });        }catch (Exception exe){

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
