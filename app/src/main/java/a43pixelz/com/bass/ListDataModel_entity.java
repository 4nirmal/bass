package a43pixelz.com.bass;

/**
 * Created by Nirmal on 9/22/2018.
 */

public class ListDataModel_entity {

    String name;
    String id;
    String version_number;
    String feature;

    public ListDataModel_entity(String name, String id, String version_number, String feature ) {
        this.name=name;
        this.id=id;
        this.version_number=version_number;
        this.feature=feature;

    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getVersion_number() {
        return version_number;
    }

    public String getFeature() {
        return feature;
    }

}