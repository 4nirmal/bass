package a43pixelz.com.bass;

/**
 * Created by Nirmal on 9/22/2018.
 */

public class ListDataModel_admin {

    String name;
    String id;
    String adminid;
    String feature;

    public ListDataModel_admin(String name, String id, String adminid, String feature ) {
        this.name=name;
        this.id=id;
        this.adminid=adminid;
        this.feature=feature;

    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getAdminid() {
        return adminid;
    }

    public String getFeature() {
        return feature;
    }

}