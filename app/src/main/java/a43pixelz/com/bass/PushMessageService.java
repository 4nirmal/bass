package a43pixelz.com.bass;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import a43pixelz.com.bass.pojo.Admin.Admin;
import a43pixelz.com.bass.pojo.Admin.AdminBody;
import a43pixelz.com.bass.pojo.Notification.Notification;
import a43pixelz.com.bass.pojo.Notification.NotificationBody;
import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PushMessageService extends Service {
    public PushMessageService() {
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }
    @Override
    public IBinder onBind(Intent intent) {

        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
    SPref sPref;
    APIInterface apiInterface;
    Handler mHandler;
    @Override
    public void onCreate() {
        Toast.makeText(getApplicationContext(), "Service started", Toast.LENGTH_SHORT).show();
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                Toast.makeText(PushMessageService.this, "Service refreshed", Toast.LENGTH_SHORT).show();
            }
        };
        sPref=new SPref(getApplicationContext());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Timer t=new Timer();
        TimerTask task=new TimerTask() {
            @Override
            public void run() {
                if(sPref.getValue("admin_login").matches("yes")){
                    try{
                    Call<Admin> call2 = apiInterface.getAdminnotifyById(sPref.getValue("admin_id"));
                    call2.enqueue(new Callback<Admin>() {
                        @Override
                        public void onResponse(Call<Admin> call, Response<Admin> response) {
                            Admin resource = response.body();
                            List<AdminBody> datumList = resource.getBody();

                            for (AdminBody datum : datumList) {
                                sPref.setValue("notification",datum.getlastnotify());
                            }
                        }

                        @Override
                        public void onFailure(Call<Admin> call, Throwable t) {
                            call.cancel();
                        }
                    });
                    }catch (Exception exe){

                    }
                }else if(sPref.getValue("user_login").matches("yes")){
                    try{
                    Call<User> call2 = apiInterface.getUsernotifyById(sPref.getValue("user_id"));
                    call2.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            User resource = response.body();
                            //Integer count = resource.getCount();
                            List<UserBody> datumList = resource.getBody();

                            for (UserBody datum : datumList) {
                                sPref.setValue("notification",datum.getlastnotify());
                            }

                        } @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            call.cancel();
                        }
                    });
                    }catch (Exception exe){

                    }

                }
                if(!sPref.getValue("notification").matches("")&&!sPref.getValue("notification").matches("null")){
                   Log.e("puss",sPref.getValue("notification"));
                   try{
                    Call<Notification> call = apiInterface.getNotificationBynotify(Integer.valueOf(sPref.getValue("notification")));
                    call.enqueue(new Callback<Notification>() {
                        @Override
                        public void onResponse(Call<Notification> call, Response<Notification> response) {
                            Notification resource = response.body();
                            // Integer count = resource.getCount();
                            List<NotificationBody> datumList = resource.getBody();
                            if(sPref.getValue("admin_login").matches("yes")){
                                for (NotificationBody datum : datumList) {
                                    if(datum.getReceiverId().contains(sPref.getValue("admin_entity"))||datum.getReceiverId().contains("All")){
                                        if(Integer.parseInt(datum.getId())>Integer.parseInt(sPref.getValue("notification"))){
                                            String messages = datum.getNotificationMessage();
                                            String titles = datum.getNotificationTitle();
                                            PushMessage pushMessage=new PushMessage();
                                            pushMessage.createNotification(titles,messages,NotificationActivity.class,getApplicationContext(),String.valueOf(Integer.valueOf(datum.getId())+43));
                                            sPref.setValue("notification",datum.getId());
                                        }
                                    }
                                }
                                AdminBody u = new AdminBody();
                                u.setUserid(sPref.getValue("admin_id"));
                                u.setlastnotify(sPref.getValue("notification"));
                                Call<AdminBody> call3 = apiInterface.updateAdminnotify(u);
                                call3.enqueue(new Callback<AdminBody>() {
                                    @Override
                                    public void onResponse(Call<AdminBody> call, Response<AdminBody> response) {
                                    }

                                    @Override
                                    public void onFailure(Call<AdminBody> call, Throwable t) {
                                    }
                                });
                            }else if(sPref.getValue("user_login").matches("yes")){

                                for (NotificationBody datum : datumList) {
                                    String sd=datum.getReceiverId();
                                    if(datum.getReceiverId().contains(sPref.getValue("user_entity"))||datum.getReceiverId().contains(sPref.getValue("user_id"))||datum.getReceiverId().contains("All")){
                                        if(Integer.parseInt(datum.getId())>Integer.parseInt(sPref.getValue("notification"))){
                                            String messages = datum.getNotificationMessage();
                                            String titles = datum.getNotificationTitle();
                                            PushMessage pushMessage=new PushMessage();
                                            pushMessage.createNotification(titles,messages,NotificationActivity.class,getApplicationContext(),String.valueOf(Integer.valueOf(datum.getId())+43));
                                            sPref.setValue("notification",datum.getId());
                                        }
                                    }
                                }
                                UserBody u = new UserBody();
                                u.setUserid(sPref.getValue("user_id"));
                                u.setlastnotify(sPref.getValue("notification"));
                                Call<UserBody> call3 = apiInterface.updateUsernotify(u);
                                call3.enqueue(new Callback<UserBody>() {
                                    @Override
                                    public void onResponse(Call<UserBody> call, Response<UserBody> response) {
                                    }

                                    @Override
                                    public void onFailure(Call<UserBody> call, Throwable t) {
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(Call<Notification> call, Throwable t) {
                            call.cancel();
                        }
                    });
                   }catch (Exception exe){

                   }
                }else{

                }

            }
        };
        t.scheduleAtFixedRate(task,0,60000);
        super.onCreate();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        try{
            Intent restartService=new Intent(getApplicationContext(),PushMessageService.class);
            restartService.setPackage(getPackageName());
            startService(restartService);}
        catch (Exception e){

        }
        super.onTaskRemoved(rootIntent);
    }


}
