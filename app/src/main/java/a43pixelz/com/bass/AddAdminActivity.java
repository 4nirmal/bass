package a43pixelz.com.bass;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import a43pixelz.com.bass.pojo.Admin.Admin;
import a43pixelz.com.bass.pojo.Admin.AdminBody;
import a43pixelz.com.bass.pojo.Entity.Body;
import a43pixelz.com.bass.pojo.Entity.Entity;
import a43pixelz.com.bass.pojo.Mailer.SendMail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAdminActivity extends AppCompatActivity {
    TextView Aname, Auserid, Aemail, Apassword, Alocation,Aphone;
    Spinner  Aentity;
    Button add, update;
    APIInterface apiInterface;
    String userid="";
    String entityid;
    ArrayAdapter<String> adapt;
    ArrayList<String> entities;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_add_admin);
        Aname = findViewById(R.id.admin_name_txt);
        Auserid = findViewById(R.id.admin_id_txt);
        Aemail = findViewById(R.id.admin_email_txt);
        Apassword = findViewById(R.id.admin_password_txt);
        Aentity = findViewById(R.id.admin_entity_txt);
        Alocation = findViewById(R.id.admin_location_txt);
        add = findViewById(R.id.admin_add_btn);
        Aphone = findViewById(R.id.admin_phone_txt);
        update = findViewById(R.id.admin_update_btn);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        //load spinner
        entities=new ArrayList<String>();
        try {
            Call<Entity> call = apiInterface.doGetListResources();
            call.enqueue(new Callback<Entity>() {
                @Override
                public void onResponse(Call<Entity> call, Response<Entity> response) {
                    Entity resource = response.body();
                    List<Body> datumList = resource.getBody();
                    for (Body datum : datumList) {
                        entities.add(datum.getEntityid());
                        adapt.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<Entity> call, Throwable t) {
                    call.cancel();
                }
            });
        }catch (Exception e){

        }
        adapt=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,entities);
        adapt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Aentity.setAdapter(adapt);

        Bundle extras = getIntent().getExtras();
        if(extras!=null){
        userid = extras.getString("admin_id");
        entityid=extras.getString("entity_id");
        if (!userid.isEmpty()) {
            getSupportActionBar().setTitle(userid);
            update.setEnabled(true);
            add.setEnabled(false);
            Auserid.setText(userid);
            add.setVisibility(View.GONE);
            loadUserDetails();
        }
        } else {
            getSupportActionBar().setTitle("Add Admin");
            add.setVisibility(View.VISIBLE);
            update.setVisibility(View.GONE);
            Apassword.setText(random(8));

        }


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                String name = Aname.getText().toString();
                String email = Aemail.getText().toString();
                String pass = Apassword.getText().toString();
                String entity = Aentity.getSelectedItem().toString();
                String loc = Alocation.getText().toString();
                String phone = Aphone.getText().toString();

                AdminBody u = new AdminBody();
                u.setName(name);
                u.setEmail(email);
                u.setPassword(pass);
                u.setEntity(entity);
                u.setLocation(loc);
                u.setPhone(phone);
                u.setUserid(userid);
                try{
                Call<AdminBody> call = apiInterface.createAdmin(u);
                call.enqueue(new Callback<AdminBody>() {
                    @Override
                    public void onResponse(Call<AdminBody> call, Response<AdminBody> response) {
                        Toast.makeText(getApplicationContext(), "Admin Added", Toast.LENGTH_SHORT).show();
                        String wmessage="<h5>Hi "+name+",</h5><br><h2 style='float:center;'>WELCOME</h2><hr><br> <p>Your profile has been created for this mail id "+email+".</p><br> Your password is "+pass +" .</p><br><br><p>Thanks,</p><br><p>BASS</p>";
                        SendMail sm = new SendMail(AddAdminActivity.this, email, "Your profile created", wmessage);
                        sm.execute();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<AdminBody> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e("callback",t.getMessage());
                    }
                });        }catch (Exception e){

            }

            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = Aname.getText().toString();
                String email = Aemail.getText().toString();
                String pass = Apassword.getText().toString();
                String entity = Aentity.getSelectedItem().toString();
                String loc = Alocation.getText().toString();
                String phone = Aphone.getText().toString();

                AdminBody u = new AdminBody();
                u.setName(name);
                u.setEmail(email);
                u.setPassword(pass);
                u.setEntity(entity);
                u.setLocation(loc);
                u.setPhone(phone);
                u.setUserid(userid);
                try{
                Call<AdminBody> call = apiInterface.updateAdmin(u);
                call.enqueue(new Callback<AdminBody>() {
                    @Override
                    public void onResponse(Call<AdminBody> call, Response<AdminBody> response) {
                        Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<AdminBody> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e("callback",t.getMessage());
                    }
                });        }catch (Exception e){

                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
    private static final String ALLOWED_CHARACTERS ="0123456789QWERTYUIOPASDFGHJKLZXCVBNM!@#$%*()qwertyuiopasdfghjklzxcvbnm";

    private static String random(final int sizeOfRandomString)
    {
        final Random random=new Random();
        final StringBuilder sb=new StringBuilder(sizeOfRandomString);
        for(int i=0;i<sizeOfRandomString;++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }

    private void loadUserDetails() {
        try{
        Call<Admin> call = apiInterface.getAdminById(userid);
        call.enqueue(new Callback<Admin>() {
            @Override
            public void onResponse(Call<Admin> call, Response<Admin> response) {
                AdminBody data = response.body().getBody().get(0);
                Aname.setText(data.getName());
                Aemail.setText(data.getEmail());
                Apassword.setText(data.getPassword());
                Aentity.setSelection(adapt.getPosition(data.getEntity()));
                Alocation.setText(data.getLocation());
                Aphone.setText(data.getPhone());
            }

            @Override
            public void onFailure(Call<Admin> call, Throwable t) {

            }
        });        }catch (Exception e){

        }
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

