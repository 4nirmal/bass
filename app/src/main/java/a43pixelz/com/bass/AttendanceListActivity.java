package a43pixelz.com.bass;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.applandeo.materialcalendarview.EventDay;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import a43pixelz.com.bass.pojo.Admin.Admin;
import a43pixelz.com.bass.pojo.Admin.AdminBody;
import a43pixelz.com.bass.pojo.Atendance.Attendance;
import a43pixelz.com.bass.pojo.Atendance.AttendanceBody;
import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendanceListActivity extends AppCompatActivity {

    APIInterface apiInterface;
    ArrayList<ListDataModal_attendance> dataModels;
    ListView listView;
    ArrayList<ListDataModel_user> UserDataModels;
    private static CustomAdapter_attendance adapter;

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_list);
        getSupportActionBar().setTitle("Attendance");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        getUsers();
        listView=(ListView)findViewById(R.id.attendance_list);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String Date = dateFormat.format(date);
        try{
        Call<Attendance> call = apiInterface.getAttendanceByDate(Date);
        call.enqueue(new Callback<Attendance>() {
            @Override
            public void onResponse(Call<Attendance> call, Response<Attendance> response) {
                Attendance resource = response.body();
                List<AttendanceBody> datumList = resource.getBody();
                dataModels = new ArrayList<>();

                for (AttendanceBody datum : datumList) {
                    String dateStr = datum.getDatestr();
                    dataModels.add(new ListDataModal_attendance("", datum.getIntime(), datum.getOuttime(), datum.getLocation(),
                            datum.getOutLocation(), datum.getUserid()));
                }
                adapter= new CustomAdapter_attendance(dataModels, UserDataModels ,getApplicationContext());

                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Attendance> call, Throwable t) {

            }
        });}catch (Exception exe){

        }

    }

    void getUsers(){
        try{
        Call<User> call = apiInterface.doGetListUser();
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d("TAG",response.code()+"");
                UserDataModels = new ArrayList<>();
                String displayResponse = "";
                User resource = response.body();
                Integer count = resource.getCount();
                List<UserBody> datumList = resource.getBody();

                for (UserBody datum : datumList) {
                    String name = datum.getName();
                    String id = datum.getUserid();
                    UserDataModels.add(new ListDataModel_user(name, id, "", ""));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                call.cancel();
            }
        });}catch (Exception exe){

        }

    }

    }
