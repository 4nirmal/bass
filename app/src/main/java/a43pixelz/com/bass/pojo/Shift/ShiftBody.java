package a43pixelz.com.bass.pojo.Shift;

/**
 * Created by Nirmal on 9/22/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShiftBody {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("shift_from")
    @Expose
    private String shiftFrom;
    @SerializedName("shift_to")
    @Expose
    private String shiftTo;
    @SerializedName("shift_name")
    @Expose
    private String shiftName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShiftFrom() {
        return shiftFrom;
    }

    public void setShiftFrom(String shiftFrom) {
        this.shiftFrom = shiftFrom;
    }

    public String getShiftTo() {
        return shiftTo;
    }

    public void setShiftTo(String shiftTo) {
        this.shiftTo = shiftTo;
    }

    public String getShiftName() {
        return shiftName;
    }

    public void setShiftName(String shiftName) {
        this.shiftName = shiftName;

}
}
