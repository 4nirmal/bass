package a43pixelz.com.bass.pojo.Entity;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Entity {

    @SerializedName("body")
    @Expose
    private List<Body> body = null;
    @SerializedName("count")
    @Expose
    private Integer count;

    public List<Body> getBody() {
        return body;
    }

    public void setBody(List<Body> body) {
        this.body = body;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}