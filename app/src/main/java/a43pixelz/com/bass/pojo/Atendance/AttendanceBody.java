package a43pixelz.com.bass.pojo.Atendance;

/**
 * Created by Nirmal on 10/4/2018.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttendanceBody {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("datestr")
    @Expose
    private String datestr;

    @SerializedName("entityid")
    @Expose
    private String entityid;

    @SerializedName("userid")
    @Expose
    private String userid;

    @SerializedName("intime")
    @Expose
    private String intime;

    @SerializedName("outtime")
    @Expose
    private String outtime;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("outlocation")
    @Expose
    private String outlocation;

    @SerializedName("backuplocation")
    @Expose
    private String backuplocation;

    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDatestr() {
        return datestr;
    }

    public void setDatestr(String datestr) {
        this.datestr = datestr;
    }

    public String getEntityid() {
        return entityid;
    }

    public void setEntityid(String entityid) {
        this.entityid = entityid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getIntime() {
        return intime;
    }

    public void setIntime(String intime) {
        this.intime = intime;
    }

    public String getOuttime() {
        return outtime;
    }

    public void setOuttime(String outtime) {
        this.outtime = outtime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOutLocation() {
        return outlocation;
    }

    public void setOutLocation(String outlocation) {
        this.outlocation = outlocation;
    }

    public String getbackupLocation() {
        return outlocation;
    }

    public void setbackupLocation(String backuplocation) {
        this.backuplocation = backuplocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
