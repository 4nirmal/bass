package a43pixelz.com.bass.pojo.Atendance;

/**
 * Created by Nirmal on 10/4/2018.
 */

       import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class Attendance {

    @SerializedName("body")
    @Expose
    private List<AttendanceBody> body = null;
    @SerializedName("count")
    @Expose
    private Integer count;

    public List<AttendanceBody> getBody() {
        return body;
    }

    public void setBody(List<AttendanceBody> body) {
        this.body = body;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}