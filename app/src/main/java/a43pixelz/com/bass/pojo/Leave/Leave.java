package a43pixelz.com.bass.pojo.Leave;

/**
 * Created by Nirmal on 10/4/2018.
 */
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

        import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class Leave {

    @SerializedName("body")
    @Expose
    private List<LeaveBody> body = null;
    @SerializedName("count")
    @Expose
    private Integer count;

    public List<LeaveBody> getBody() {
        return body;
    }

    public void setBody(List<LeaveBody> body) {
        this.body = body;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}