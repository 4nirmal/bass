package a43pixelz.com.bass.pojo.Shift;

/**
 * Created by Nirmal on 9/22/2018.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Shift {

    @SerializedName("body")
    @Expose
    private List<ShiftBody> body = null;
    @SerializedName("count")
    @Expose
    private Integer count;

    public List<ShiftBody> getBody() {
        return body;
    }

    public void setBody(List<ShiftBody> body) {
        this.body = body;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
