package a43pixelz.com.bass.pojo.Admin;

/**
 * Created by Nirmal on 9/22/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdminBody {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("userid")
    @Expose
    private String userid;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("entity")
    @Expose
    private String entity;

    @SerializedName("phone")
    @Expose
    private String phone;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @SerializedName("lastnotify")
    @Expose
    private String lastnotify;

    public String getlastnotify() {
        return lastnotify;
    }

    public void setlastnotify(String lastnotify) {
        this.lastnotify = lastnotify;
    }
}