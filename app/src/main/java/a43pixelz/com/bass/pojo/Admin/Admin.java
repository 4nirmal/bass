package a43pixelz.com.bass.pojo.Admin;

/**
 * Created by Nirmal on 9/22/2018.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Admin {

    @SerializedName("body")
    @Expose
    private List<AdminBody> body = null;
    @SerializedName("count")
    @Expose
    private Integer count;

    public List<AdminBody> getBody() {
        return body;
    }

    public void setBody(List<AdminBody> body) {
        this.body = body;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
