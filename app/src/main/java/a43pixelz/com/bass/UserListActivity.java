package a43pixelz.com.bass;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserListActivity extends AppCompatActivity {
    Intent userView;
    APIInterface apiInterface;
    ArrayList<ListDataModel_user> dataModels;
    ListView listView;
    private static CustomAdapter_user adapter;
    Intent UserView;
    String Entity_ID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("User List");
        listView=findViewById(R.id.user_list);
        apiInterface=APIClient.getClient().create(APIInterface.class);
        UserView=new Intent(this,CalenderViewActivity.class);
        Entity_ID="";
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            Entity_ID=extras.getString("entity_id");
        }
        else {

        }
        try{
        Call<User> call = apiInterface.doGetListUser();
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d("TAG",response.code()+"");
                dataModels = new ArrayList<>();
                String displayResponse = "";
                User resource = response.body();
                Integer count = resource.getCount();
                List<UserBody> datumList = resource.getBody();

                if(Entity_ID=="") {
                for (UserBody datum : datumList) {
                        String name = datum.getName();
                        String id = datum.getUserid();
                        dataModels.add(new ListDataModel_user(name, id, "", ""));
                }
                }
                else {
                    for (UserBody datum : datumList) {
                        if(datum.getUserid().matches(Entity_ID)) {
                            String name = datum.getName();
                            String id = datum.getEntity();
                            dataModels.add(new ListDataModel_user(name, id, "", ""));
                        }
                    }
                }
                adapter= new CustomAdapter_user(dataModels,getApplicationContext());

                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        ListDataModel_user dataModel= dataModels.get(position);
                        UserView.putExtra("User_id", dataModel.getId());
                        startActivity(UserView);
//                        Snackbar.make(view, dataModel.getName()+"\n"+dataModel.getId()+" API: "+dataModel.getVersion_number(), Snackbar.LENGTH_LONG)
//                                .setAction("No action", null).show();
                    }
                });

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                call.cancel();
            }
        });
        }catch (Exception exe){

        }
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
