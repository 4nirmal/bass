package a43pixelz.com.bass;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Nirmal on 6/17/2018.
 */

public class SPref {
    Context ctx;

    public  SPref(Context ctx){
        this.ctx = ctx;
    }

    public void setValue(String Key, String Value){
        SharedPreferences.Editor editor = ctx.getSharedPreferences("bass", MODE_PRIVATE).edit();
        editor.putString(Key, Value);
        editor.apply();
    }

    public String getValue(String Key){
        SharedPreferences prefs = ctx.getSharedPreferences("bass", MODE_PRIVATE);
        return prefs.getString(Key, "null");
    }
    public String getKey(String Key){
        SharedPreferences prefs = ctx.getSharedPreferences("bass", MODE_PRIVATE);
        Map<String, ?> allEntries = prefs.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            if(entry.getKey().contains(Key)){
                return entry.getKey();
            }
        }
            return "";


    }
    public void deleteSpref(){
        SharedPreferences prefs = ctx.getSharedPreferences("bass", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
    }
}
