package a43pixelz.com.bass;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import a43pixelz.com.bass.pojo.Entity.Body;
import a43pixelz.com.bass.pojo.Entity.Entity;
import a43pixelz.com.bass.pojo.Notification.Notification;
import a43pixelz.com.bass.pojo.Notification.NotificationBody;
import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {
    SPref sPref;
    AlertDialog alert;
    APIInterface apiInterface;
    ListView listView;
    FloatingActionButton faab;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        faab=findViewById(R.id.floatingActionButton);
        sPref=new SPref(this);
        if(sPref.getValue("superuser_login").matches("yes")){
            faab.setVisibility(View.VISIBLE);
        }else if(sPref.getValue("admin_login").matches("yes")){
            faab.setVisibility(View.VISIBLE);
        }else{
            faab.setVisibility(View.INVISIBLE);
        }
        getSupportActionBar().setTitle("Notifications");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView=findViewById(R.id.notification_listview);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView2 = li.inflate(R.layout.notification,null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView2);

        alert = alertDialogBuilder.create();
        alert.setCancelable(true);
        faab.setOnClickListener(v -> {
            alert.show();
            alertView();
        });
        try{
        Call<Notification> call = apiInterface.doGetListNotification();
        call.enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {
                Log.d("TAG",response.code()+"");
                ArrayList<ListDataModel_notify> dataModels = new ArrayList<>();
                String displayResponse = "";
                Notification resource = response.body();
                Integer count = resource.getCount();
                List<NotificationBody> datumList = resource.getBody();
                if(sPref.getValue("superuser_login").matches("yes")){
                    for (NotificationBody datum : datumList) {
                        String dates = datum.getCreatedAt();
                        String messages = datum.getNotificationMessage();
                        String titles = datum.getNotificationTitle();
                        dataModels.add(new ListDataModel_notify(messages, dates,titles));
                    }
                }else if(sPref.getValue("admin_login").matches("yes")){
                    for (NotificationBody datum : datumList) {
                        if(datum.getReceiverId().contains(sPref.getValue("admin_entity"))||datum.getReceiverId().contains("All")){

                            String dates = datum.getCreatedAt();
                            String messages = datum.getNotificationMessage();
                            String titles = datum.getNotificationTitle();
                            dataModels.add(new ListDataModel_notify(messages, dates,titles));
                        }
                    }
                }else{
                    for (NotificationBody datum : datumList) {
                        if(datum.getReceiverId().contains(sPref.getValue("user_entity"))||datum.getReceiverId().contains(sPref.getValue("user_id"))||datum.getReceiverId().contains("All")){

                            String dates = datum.getCreatedAt();
                            String messages = datum.getNotificationMessage();
                            String titles = datum.getNotificationTitle();
                            dataModels.add(new ListDataModel_notify(messages, dates,titles));
                        }
                    }
                }
                CustomAdapter_notify adapter= new CustomAdapter_notify(dataModels,getApplicationContext());

                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();


            }

            @Override
            public void onFailure(Call<Notification> call, Throwable t) {
                call.cancel();
            }
        });}catch (Exception exe){

        }
    }
    void alertView(){
        title=alert.findViewById(R.id.title);
        message=alert.findViewById(R.id.message);
        send=alert.findViewById(R.id.send);
        spinner=alert.findViewById(R.id.spinner);
        all=alert.findViewById(R.id.all);
        entity=alert.findViewById(R.id.entity);
        user=alert.findViewById(R.id.user);
        all.setOnClickListener(this::radioclicked);
        entity.setOnClickListener(this::radioclicked);
        user.setOnClickListener(this::radioclicked);
        send.setOnClickListener(view -> {
            if(all.isChecked()&&!title.getText().toString().matches("")&&!message.getText().toString().matches("")){
                NotificationBody n=new NotificationBody();
                n.setType("All");
                n.setNotificationTitle(title.getText().toString());
                n.setNotificationMessage(message.getText().toString());
                n.setReceiverId("All");
                try{
                Call<NotificationBody> call = apiInterface.createNotification(n);
                call.enqueue(new Callback<NotificationBody>() {
                    @Override
                    public void onResponse(Call<NotificationBody> call, Response<NotificationBody> response) {
                        Toast.makeText(getApplicationContext(), "Message sent", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<NotificationBody> call, Throwable t) {
                        call.cancel();
                    }
                });
                }catch (Exception exe){

                }
            }else if(entity.isChecked()&&!title.getText().toString().matches("")&&!message.getText().toString().matches("")){
                NotificationBody n=new NotificationBody();
                n.setType("Entity");
                n.setNotificationTitle(title.getText().toString());
                n.setNotificationMessage(message.getText().toString());
                n.setReceiverId(spinner.getSelectedItem().toString());
                try{
                Call<NotificationBody> call = apiInterface.createNotification(n);
                call.enqueue(new Callback<NotificationBody>() {
                    @Override
                    public void onResponse(Call<NotificationBody> call, Response<NotificationBody> response) {
                        Toast.makeText(getApplicationContext(), "Message sent", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<NotificationBody> call, Throwable t) {
                        call.cancel();
                    }
                });
                }catch (Exception exe){

                }
            }else if(user.isChecked()&&!title.getText().toString().matches("")&&!message.getText().toString().matches("")){
                NotificationBody n=new NotificationBody();
                n.setType("User");
                n.setNotificationTitle(title.getText().toString());
                n.setNotificationMessage(message.getText().toString());
                n.setReceiverId(spinner.getSelectedItem().toString());
                try{
                Call<NotificationBody> call = apiInterface.createNotification(n);
                call.enqueue(new Callback<NotificationBody>() {
                    @Override
                    public void onResponse(Call<NotificationBody> call, Response<NotificationBody> response) {
                        Toast.makeText(getApplicationContext(), "Message sent", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<NotificationBody> call, Throwable t) {
                        call.cancel();
                    }
                });
                }catch (Exception exe){

                }
            }else{
                Toast.makeText(this, "Wrong Entry", Toast.LENGTH_SHORT).show();
            }
        });
        all.setChecked(true);
        spinner.setVisibility(View.GONE);
        entity.setChecked(false);
        user.setChecked(false);
        if(sPref.getValue("superuser_login").matches("yes")){

        }else if(sPref.getValue("admin_login").matches("yes")){
            all.setVisibility(View.GONE);
        }else{
            all.setVisibility(View.GONE);
            entity.setVisibility(View.GONE);
            user.setVisibility(View.GONE);
        }
    }

    EditText title;
    EditText message;
    Button send;
    Spinner spinner;
    RadioButton all;
    RadioButton entity;
    RadioButton user;
    private void radioclicked(View view) {
        switch (view.getId()){
            case R.id.all:
                spinner.setVisibility(View.GONE);
                entity.setChecked(false);
                user.setChecked(false);
                break;
            case R.id.entity:
                spinner.setVisibility(View.VISIBLE);
                try{
                Call<Entity> call = apiInterface.doGetListResources();
                call.enqueue(new Callback<Entity>() {
                    @Override
                    public void onResponse(Call<Entity> call, Response<Entity> response) {
                        Entity resource = response.body();
                        List<Body> datumList = resource.getBody();
                        List<String> entities=new ArrayList<>();
                        for (Body datum : datumList) {
                            String name = datum.getName();
                            String id = datum.getEntityid();
                            entities.add(name+","+id);
                        }
                        ArrayAdapter<String> adapt=new ArrayAdapter<>(alert.getContext(),android.R.layout.simple_spinner_dropdown_item,entities);
                        adapt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapt);
                    }
                    @Override
                    public void onFailure(Call<Entity> call, Throwable t) {
                        call.cancel();
                    }
                });
                }catch (Exception exe){

                }
                all.setChecked(false);
                user.setChecked(false);
                break;
            case R.id.user:
                spinner.setVisibility(View.VISIBLE);
                try{
                Call<User> call2 = apiInterface.doGetListUser();
                call2.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call2, Response<User> response) {
                        User resource = response.body();
                        List<String> entities=new ArrayList<String>();
                        List<UserBody> datumList = resource.getBody();

                            for (UserBody datum : datumList) {
                                String name = datum.getName();
                                String id = datum.getUserid();
                                entities.add(name+","+id);
                             }
                         ArrayAdapter<String> adapt=new ArrayAdapter<String>(alert.getContext(),android.R.layout.simple_spinner_dropdown_item,entities);
                        adapt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapt);
                    }

                    @Override
                    public void onFailure(Call<User> call2, Throwable t) {
                        call2.cancel();
                    }
                });
                }catch (Exception exe){

                }
                entity.setChecked(false);
                all.setChecked(false);
                break;
        }
    }

}
