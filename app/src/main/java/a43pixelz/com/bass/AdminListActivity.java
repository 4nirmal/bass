package a43pixelz.com.bass;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import a43pixelz.com.bass.pojo.Admin.Admin;
import a43pixelz.com.bass.pojo.Admin.AdminBody;
import a43pixelz.com.bass.pojo.Entity.Body;
import a43pixelz.com.bass.pojo.Entity.Entity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminListActivity extends AppCompatActivity {

    APIInterface apiInterface;
    ArrayList<ListDataModel_admin> dataModels;
    ListView listView;
    Intent adminView;
    private static CustomAdapter_admin adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_list);
        getSupportActionBar().setTitle("Admin List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adminView = new Intent(this, AddAdminActivity.class);

        listView=(ListView)findViewById(R.id.admin_list);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        /**
         GET List Resources
         **/
        try{
        Call<Admin> call = apiInterface.doGetListAdmin();
        call.enqueue(new Callback<Admin>() {
            @Override
            public void onResponse(Call<Admin> call, Response<Admin> response) {
                Log.d("TAG",response.code()+"");
                dataModels = new ArrayList<>();
                String displayResponse = "";
                Admin resource = response.body();
                Integer count = resource.getCount();
                List<AdminBody> datumList = resource.getBody();


                for (AdminBody datum : datumList) {
                    String name = datum.getName();
                    String id = datum.getEntity();
                    dataModels.add(new ListDataModel_admin(name, id, datum.getUserid(), ""));
                }

                adapter= new CustomAdapter_admin(dataModels,getApplicationContext());

                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        ListDataModel_admin dataModel= dataModels.get(position);
                        adminView.putExtra("admin_id", dataModel.getAdminid());
                        adminView.putExtra("entity_id", dataModel.getId());
                        startActivity(adminView);
//                        Snackbar.make(view, dataModel.getName()+"\n"+dataModel.getId()+" API: "+dataModel.getVersion_number(), Snackbar.LENGTH_LONG)
//                                .setAction("No action", null).show();
                    }
                });

            }

            @Override
            public void onFailure(Call<Admin> call, Throwable t) {
                call.cancel();
            }
        });
        }catch (Exception exe){

        }

    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.admin_list_menu, menu);
        return true;
    }
    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
