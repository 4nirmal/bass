package a43pixelz.com.bass;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import a43pixelz.com.bass.pojo.Admin.AdminBody;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends AppCompatActivity {
    SPref sPref;
    TextView userid,entity,macaddress,shift,username;
    EditText email,phone,password,location;
    RadioButton male,female,other;
    Button update;
    APIInterface apiInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        sPref=new SPref(this);
        userid=findViewById(R.id.user_Id);
        username=findViewById(R.id.user_name);
        email=findViewById(R.id.user_email);
        phone=findViewById(R.id.user_phone);
        location=findViewById(R.id.user_location);
        entity=findViewById(R.id.user_entity);
        password=findViewById(R.id.user_password);
        macaddress=findViewById(R.id.user_mac_address);
        male=findViewById(R.id.radio_male);
        female=findViewById(R.id.radio_female);
        other=findViewById(R.id.radio_other);
        shift=findViewById(R.id.user_shift);
        update=findViewById(R.id.update_profile);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        male.setOnClickListener(this::onRadioButtonClicked);
        female.setOnClickListener(this::onRadioButtonClicked);
        other.setOnClickListener(this::onRadioButtonClicked);

        getSupportActionBar().setTitle("Profile: "+sPref.getValue("user_id"));
        getSupportActionBar().setSubtitle(sPref.getValue("user_name"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userid.setText(sPref.getValue("user_id"));
        username.setText(sPref.getValue("user_name"));
        email.setText(sPref.getValue("user_email"));
        phone.setText(sPref.getValue("user_phone"));
        entity.setText(sPref.getValue("user_entity"));
        password.setText(sPref.getValue("user_password"));
        location.setText(sPref.getValue("user_location"));
        macaddress.setText(sPref.getValue("user_mac_address"));
        shift.setText(sPref.getValue("user_shift"));
        switch (sPref.getValue("user_gender")){
            case "male":
                male.setChecked(true);
                break;
            case "female":
                female.setChecked(true);
                break;
            case "other":
                other.setChecked(true);
                break;
        }
update.setOnClickListener(v->{
    UserBody u = new UserBody();
    u.setName(username.getText().toString());
    u.setEmail(email.getText().toString());
    u.setPassword(password.getText().toString());
    u.setDescription(sPref.getValue("user_description"));
    u.setShift(sPref.getValue("user_shift"));
    u.setEntity(sPref.getValue("user_entity"));
    u.setLocation(location.getText().toString());
    u.setPhone(phone.getText().toString());
    u.setMac_address(sPref.getValue("user_mac_address"));
    u.setGender(sPref.getValue("user_gender"));
    u.setUserid(userid.getText().toString());
    try{
    Call<UserBody> call = apiInterface.updateUser(u);
    call.enqueue(new Callback<UserBody>() {
        @Override
        public void onResponse(Call<UserBody> call, Response<UserBody> response) {
            Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFailure(Call<UserBody> call, Throwable t) {
            //Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_LONG).show();
        }
    });
    }catch (Exception exe){

    }
});

    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.radio_male:
                female.setChecked(false);
                other.setChecked(false);
                    break;
            case R.id.radio_female:
                male.setChecked(false);
                other.setChecked(false);
                    break;
            case R.id.radio_other:
                female.setChecked(false);
                male.setChecked(false);
                    break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
