package a43pixelz.com.bass;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Explode;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import a43pixelz.com.bass.pojo.Admin.Admin;
import a43pixelz.com.bass.pojo.Admin.AdminBody;
import a43pixelz.com.bass.pojo.Entity.Body;
import a43pixelz.com.bass.pojo.Entity.Entity;
import a43pixelz.com.bass.pojo.User.User;
import a43pixelz.com.bass.pojo.User.UserBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    APIInterface apiInterface;
    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@bass.com:hello",
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    AutoCompleteTextView entity;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    SPref sPref;
    TextView logo;
    LinearLayout logo_li;
    Button mEmailSignInButton;
    Timer t1;
    GifView gifView1;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slideup);
        Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slidedown);
        Animation zoomin = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoomin);
        Animation zoomout = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoomout);
        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        sPref=new SPref(this);

        //getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        getWindow().setEnterTransition(new Explode());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        // Set up the login form.
        entity=findViewById(R.id.entity);
        mEmailView = (AutoCompleteTextView)findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);

        ArrayList<String> entities=new ArrayList<String>(){};


        logo_li=findViewById(R.id.logo_li);
        logo=findViewById(R.id.textView16);

        entity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Call<Entity> callu = apiInterface.doGetListResources();
                callu.enqueue(new Callback<Entity>() {
                    @Override
                    public void onResponse(Call<Entity> call, Response<Entity> response) {
                        Entity resource = response.body();
                        List<Body> datumList = resource.getBody();
                        entities.clear();
                        for (Body datum : datumList) {
                            if(datum.getEntityid().contains(entity.getText())){
                                entities.add(datum.getEntityid());
                            }

                        }

                        ArrayAdapter<String> entityadptr = new ArrayAdapter<String>
                                (LoginActivity.this, android.R.layout.select_dialog_item, entities);

                        entity.setAdapter(entityadptr);
                        entityadptr.notifyDataSetChanged();

                    }
                    @Override
                    public void onFailure(Call<Entity> call, Throwable t) {
                        call.cancel();
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });





        populateAutoComplete();
        mProgressView = findViewById(R.id.login_progress);
        mProgressView.setZ(10000);
        gifView1 = (GifView) findViewById(R.id.gif1);
        gifView1.setGifResource(R.drawable.myanimation);
        gifView1.play();
        t1=new Timer();
        mEmailSignInButton.setVisibility(View.INVISIBLE);
        TimerTask task=new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(()->gifView1.pause());

                if(sPref.getValue("admin_login").matches("yes")){
                    String email=sPref.getValue("admin_email");
                    String password=sPref.getValue("admin_password");

                    try{
                        Call<Admin> call = apiInterface.adminLogin(email, password);
                        call.enqueue(new Callback<Admin>() {
                            @Override
                            public void onResponse(Call<Admin> call, Response<Admin> response) {
                                Admin resource = response.body();
                                if(resource != null){
                                    List<AdminBody> datumList = resource.getBody();
                                    for (AdminBody datum : datumList) {
                                        //Toast.makeText(LoginActivity.this, "Admin Check", Toast.LENGTH_SHORT).show();
                                        sPref.setValue("admin_id",datum.getUserid());
                                        sPref.setValue("admin_entity",datum.getEntity());
                                        sPref.setValue("admin_name",datum.getName());
                                        sPref.setValue("admin_password",datum.getPassword());
                                        sPref.setValue("admin_email",datum.getEmail());
                                        sPref.setValue("admin_phone",datum.getPhone());
                                        sPref.setValue("admin_login","yes");
                                        showProgress(false);
                                        Intent intent1=new Intent(getApplicationContext(),PushMessageService.class);
                                        intent1.setPackage(getPackageName());
                                        startService(intent1);
                                        startActivity(new Intent(LoginActivity.this, AdminHomeActivity.class));
                                        finish();

                                        //initialize service

                                    }
                                }
                                else{
                                    checkUserLogin(email, password);
                                }
                            }

                            @Override
                            public void onFailure(Call<Admin> call, Throwable t) {
                                runOnUiThread(()->{
                                    ConstraintLayout con=findViewById(R.id.con);
                                    Snackbar.make(con,"Problem On login",Snackbar.LENGTH_LONG).show();
                                    Log.e("Errors",t.getMessage());
                                    gifView1.setVisibility(View.INVISIBLE);
                                    logo.startAnimation(zoomin);
                                    logo_li.startAnimation(zoomout);
                                    entity.startAnimation(slideUp);
                                    mEmailView.startAnimation(slideUp);
                                    mPasswordView.startAnimation(slideUp);
                                    mEmailSignInButton.startAnimation(slideUp);
                                    mEmailSignInButton.setVisibility(View.VISIBLE);
                                });
                            }
                        });}catch (Exception exe){

                    }
                }else if(sPref.getValue("user_login").matches("yes")){
                    String email=sPref.getValue("user_email");
                    String password=sPref.getValue("user_password");
                    apiInterface = APIClient.getClient().create(APIInterface.class);
                    try{
                        Call<User> call2 = apiInterface.userLogin(email, password);
                        call2.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                User resource = response.body();
                                Integer count = resource.getCount();
                                if(resource != null){
                                    if(count == 1){
                                        List<UserBody> datumList = resource.getBody();
                                        for (UserBody datum : datumList) {
                                            if (datum.getMac_address().matches("") || datum.getMac_address().matches(AppUtil.getMacAddr())) {
                                                sPref.setValue("user_id",datum.getUserid());
                                                sPref.setValue("user_email",datum.getEmail());
                                                sPref.setValue("user_phone",datum.getPhone());
                                                sPref.setValue("user_mac_address",AppUtil.getMacAddr());
                                                sPref.setValue("user_entity",datum.getEntity());
                                                sPref.setValue("user_gender",datum.getGender());
                                                sPref.setValue("user_name",datum.getName());
                                                //sPref.setValue("user_id",datum.getId());
                                                sPref.setValue("user_description",datum.getDescription());
                                                sPref.setValue("user_location",datum.getLocation());
                                                sPref.setValue("user_password",datum.getPassword());
                                                sPref.setValue("user_shift",datum.getShift());
                                                sPref.setValue("user_login","yes");
                                                showProgress(false);
                                                Intent intent1=new Intent(getApplicationContext(),PushMessageService.class);
                                                intent1.setPackage(getPackageName());
                                                startService(intent1);
                                                startActivity(new Intent(LoginActivity.this, UserHomeActivity.class));
                                                finish();
                                                //initialize service
                                            }
                                        }
                                    }
                                }
                                else{
                                    runOnUiThread(()->{
                                        gifView1.setVisibility(View.INVISIBLE);
                                        logo.startAnimation(zoomin);
                                        logo_li.startAnimation(zoomout);
                                        entity.startAnimation(slideUp);
                                        mEmailView.startAnimation(slideUp);
                                        mPasswordView.startAnimation(slideUp);
                                        mEmailSignInButton.startAnimation(slideUp);
                                        mEmailSignInButton.setVisibility(View.VISIBLE);
                                    });
                                }
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {

                               runOnUiThread(()->{
                                   ConstraintLayout con=findViewById(R.id.con);
                                   Snackbar.make(con,"Problem On login",Snackbar.LENGTH_LONG).show();
                                   Log.e("Errors",t.getMessage());
                                   gifView1.setVisibility(View.INVISIBLE);
                                   logo.startAnimation(zoomin);
                                   logo_li.startAnimation(zoomout);
                                   entity.startAnimation(slideUp);
                                   mEmailView.startAnimation(slideUp);
                                   mPasswordView.startAnimation(slideUp);
                                   mEmailSignInButton.startAnimation(slideUp);
                                   mEmailSignInButton.setVisibility(View.VISIBLE);
                               });
                            }
                        });}catch (Exception ignored){

                    }
                }else if(sPref.getValue("superuser_login").matches("yes")){
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                }else{
                    runOnUiThread(()->{
                        gifView1.setVisibility(View.INVISIBLE);
                        logo.startAnimation(zoomin);
                        logo_li.startAnimation(zoomout);
                        entity.startAnimation(slideUp);
                        mEmailView.startAnimation(slideUp);
                        mPasswordView.startAnimation(slideUp);
                        mEmailSignInButton.startAnimation(slideUp);
                        mEmailSignInButton.setVisibility(View.VISIBLE);
                    });
                }
            }
        };
        t1.schedule(task,5500);


        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    if(mEmailView.getText().toString().matches("su@bass.com")&&mPasswordView.getText().toString().matches("43pixelz")){
                         sPref.setValue("superuser_login","yes");

                        Intent intent1=new Intent(getApplicationContext(),PushMessageService.class);
                        intent1.setPackage(getPackageName());
                        startService(intent1);
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                        //initialize service

                    }else{
                        attemptLogin();
                    }
                    return true;
                }
                return false;
            }
        });
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ConstraintLayout mainLayout;
                mainLayout = (ConstraintLayout)findViewById(R.id.con);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
                }
                if(mEmailView.getText().toString().matches("su@bass.com")&&mPasswordView.getText().toString().matches("43pixelz")){
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    sPref.setValue("superuser_login","yes");

                    Intent intent1=new Intent(getApplicationContext(),PushMessageService.class);
                    intent1.setPackage(getPackageName());
                    startService(intent1);
                    finish();
                    //initialize service

                }else{
                    attemptLogin();
                }
            }
        });

    }



    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
//          Check admin login
            apiInterface = APIClient.getClient().create(APIInterface.class);
            try{
                Call<Admin> call = apiInterface.adminLogin(email, password);
                call.enqueue(new Callback<Admin>() {
                    @Override
                    public void onResponse(Call<Admin> call, Response<Admin> response) {
                        Admin resource = response.body();
                        if(resource != null){
                            List<AdminBody> datumList = resource.getBody();
                            for (AdminBody datum : datumList) {
                                //Toast.makeText(LoginActivity.this, "jjdsoign", Toast.LENGTH_SHORT).show();
                                sPref.setValue("admin_id",datum.getUserid());
                                sPref.setValue("admin_entity",datum.getEntity());
                                sPref.setValue("admin_name",datum.getName());
                                sPref.setValue("admin_password",datum.getPassword());
                                sPref.setValue("admin_email",datum.getEmail());
                                sPref.setValue("admin_phone",datum.getPhone());
                                sPref.setValue("admin_login","yes");
                                startActivity(new Intent(LoginActivity.this, AdminHomeActivity.class));

                                Intent intent1=new Intent(getApplicationContext(),PushMessageService.class);
                                intent1.setPackage(getPackageName());
                                startService(intent1);finish();
                                //initialize service

                            }
                        }
                        else{

                            checkUserLogin(email, password);
                        }
                    }

                    @Override
                    public void onFailure(Call<Admin> call, Throwable t) {

                        checkUserLogin(email, password);
                    }
                });}catch (Exception exe){

            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if(show){

            mEmailSignInButton.setVisibility(View.INVISIBLE);
        }else{
            mEmailSignInButton.setVisibility(View.VISIBLE);
        }
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        //addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            Boolean Result = false;
            String userType = "";
//            try {
//                // Simulate network access.
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                return false;
//            }

            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    Result = pieces[1].equals(mPassword);
                    userType = "SUPERADMIN";
                }
            }


//
//            if(mEmail.equals("admin@a.com") && mPassword.equals("admin123")){
//                Result = true;
//                userType = "ADMIN";
//            }
//
//            if(mEmail.equals("user@u.com") && mPassword.equals("user123")){
//                Result = true;
//                userType = "USER";
//            }
//

            apiInterface = APIClient.getClient().create(APIInterface.class);
            try{
                Call<Admin> call = apiInterface.adminLogin(mEmail, mPassword);
                call.enqueue(new Callback<Admin>() {
                    @Override
                    public void onResponse(Call<Admin> call, Response<Admin> response) {
                        Admin resource = response.body();
                        Integer count = resource.getCount();
                        if(count == 1) {
                            List<AdminBody> datumList = resource.getBody();
                            for (AdminBody datum : datumList) {
                                sPref.setValue("admin_id",datum.getUserid());
                                sPref.setValue("admin_entity",datum.getEntity());
                                sPref.setValue("admin_name",datum.getName());
                                sPref.setValue("admin_password",datum.getPassword());
                                sPref.setValue("admin_location",datum.getLocation());
                                sPref.setValue("admin_phone",datum.getPhone());
                                sPref.setValue("admin_email",datum.getEmail());
                                sPref.setValue("admin_login","yes");
                               showProgress(false);
                                Intent intent1=new Intent(getApplicationContext(),PushMessageService.class);
                                intent1.setPackage(getPackageName());
                                startService(intent1);
                                startActivity(new Intent(LoginActivity.this, AdminHomeActivity.class));
                                finish();
                                //initialize service

                            }
                        }else{
                            checkUserLogin(mEmail, mPassword);
                        }
                    }

                    @Override
                    public void onFailure(Call<Admin> call, Throwable t) {

                    }
                });}catch (Exception exe){

            }

            if(Result){
                switch (userType){
                    case "SUPERADMIN":
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));

                        break;
                }
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
    private void checkUserLogin(String email, String password){
        try{
            Call<User> call2 = apiInterface.userLogin(email, password);
            call2.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    User resource = response.body();
                    Integer count = resource.getCount();
                    if(resource != null){
                        if(count == 1){
                            List<UserBody> datumList = resource.getBody();
                            for (UserBody datum : datumList) {
                                if (datum.getMac_address().matches("") || datum.getMac_address().matches(AppUtil.getMacAddr())) {
                                    String name = datum.getName().toString();
                                    String email = datum.getEmail().toString();
                                    String pass = datum.getPassword().toString();
                                    String desc = datum.getDescription().toString();
                                    String entity = datum.getEntity().toString();
                                    String loc = datum.getLocation().toString();
                                    String phonee = datum.getPhone().toString();
                                    String macaddresss=AppUtil.getMacAddr().toString();
                                    String shift = datum.getShift().toString();
                                    String gender=datum.getGender();

                                    UserBody u = new UserBody();
                                    u.setUserid(datum.getUserid());
                                    u.setName(name);u.setEmail(email);u.setPassword(pass);u.setDescription(desc);u.setShift(shift);
                                    u.setEntity(entity);u.setLocation(loc);u.setPhone(phonee);u.setMac_address(macaddresss);u.setGender(gender);
                                    Call<UserBody> call2 = apiInterface.updateUser(u);
                                    call2.enqueue(new Callback<UserBody>() {
                                        @Override
                                        public void onResponse(Call<UserBody> call2, Response<UserBody> response) {
                                        }

                                        @Override
                                        public void onFailure(Call<UserBody> call2, Throwable t) {

                                        }
                                    });

                                    sPref.setValue("user_id",datum.getUserid());
                                    sPref.setValue("user_email",datum.getEmail());
                                    sPref.setValue("user_phone",datum.getPhone());
                                    sPref.setValue("user_mac_address",AppUtil.getMacAddr());
                                    sPref.setValue("user_entity",datum.getEntity());
                                    sPref.setValue("user_gender",datum.getGender());
                                    sPref.setValue("user_name",datum.getName());
                                    //sPref.setValue("user_id",datum.getId());
                                    sPref.setValue("user_description",datum.getDescription());
                                    sPref.setValue("user_location",datum.getLocation());
                                    sPref.setValue("user_password",datum.getPassword());
                                    sPref.setValue("user_shift",datum.getShift());
                                    sPref.setValue("user_login","yes");
                                    showProgress(false);
                                    Intent intent1=new Intent(getApplicationContext(),PushMessageService.class);
                                    intent1.setPackage(getPackageName());
                                    startService(intent1);
                                    startActivity(new Intent(LoginActivity.this, UserHomeActivity.class));
                                    finish();

                                }
                            }
                        }

                    }
                    else{

                        showProgress(false);
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    //Toast.makeText(LoginActivity.this, "Invalid Login details", Toast.LENGTH_SHORT).show();
                    showProgress(false);
                    ConstraintLayout con=findViewById(R.id.con);
                    Snackbar.make(con,"Invalid login details",Snackbar.LENGTH_LONG).setActionTextColor(Color.red(1)).show();

                }
            });
        }catch (Exception exe){

        }
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    // Shows the system bars by removing all the flags
// except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }
}

